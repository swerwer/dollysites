<!DOCTYPE html>
<html>
<head>
    <title>Dolly Sites</title>
    <link rel="stylesheet" href="dolly_templates/css/main.css">
    <link rel="stylesheet" href="dolly_templates/css/ztree.css">
    <meta charset="utf-8">
    <meta name="robots" content="noindex"/>
</head>

<?php $version = $this->getVersion();
$needMessage = false;

if (is_array($version)) {
    $needMessage = true;
}
?>

<body class="install_page" <?php echo ($needMessage) ? 'style="overflow: hidden"' : '';?>>

<?php if ($needMessage):?>
    <div id="blockpage" style="width: 100%; height: 100%; background-color: black; opacity: .7; z-index: 150; position: absolute; top:0"></div>
    <?php if ($version['slots'] !== 0):?>
        <div id="secure_info" style="display: block; font-size: 16px; padding: 20px; position: absolute; width: 460px; height: 160px; z-index: 160; top: 300px; left: 50%; margin-left: -230px; background-color: white; border-radius: 15px; border: 1px solid red;">
            Вы первый раз запускаете скрипт с айпи <i><?php echo $version['ip'];?></i><br>
            Чтобы продолжить работать, нужно привязать скрипт к этому адресу.
            У вас <?php echo $version['slots'];?> свободных слотов из <?php echo $version['max_slots'];?> ,
            <a href="/admin.php?action=add_ip_to_wl&ip=<?php echo $version['ip'];?>" onclick="return confirm('are you sure?');">привязать</a>?<br><br>
            <a href="<?php echo $version['url_base'];?>" target="_blank">Управление привязкой</a>
        </div>
    <?php else: ?>
        <div id="secure_info" style="font-size: 16px; padding: 20px; position: absolute; width: 460px; height: 175px; z-index: 160; top: 300px; left: 50%; margin-left: -230px; background-color: white; border-radius: 15px; border: 1px solid red;">
            Вы первый раз запускаете скрипт с айпи <i><?php echo $version['ip'];?></i><br>
            Чтобы продолжить работать, нужно привязать скрипт к этому адресу, но свободных слотов у вас нет. Можете удалить ненужный, либо докупить:<br><br>
            <a href="<?php echo $version['url_base'];?>" target="_blank" style="top:20px">Управление привязкой</a>
        </div>
    <?php endif;?>
<?php endif;?>
<div class="logo_text">
    DollySites <span style="font-size:15px;color:#7b7c7d;">v.<?php echo Constants::VERSION;?></span>
</div>
<div class="body">
    <form  action   = "../index.php?act_dolly=parse&main=true"
           method   = "post"
           id       = "form1">
        <div class="installation_steps">
            <ul>
                <li class="first active">
                    <span><?php echo $this->t('Шаг');?> 1</span>
                    <?php echo $this->t('Основные настройки');?>
                </li>
                <li class="second">
                    <span><?php echo $this->t('Шаг');?> 2</span>
                    <?php echo $this->t('Кэширование');?>
                </li>
                <li class="third">
                    <span><?php echo $this->t('Шаг');?> 3</span>
                    <?php echo $this->t('Готово');?>
                </li>
            </ul>
        </div>
        <div class="install_content">
            <div class="first_step step active">
                <div class="install_first_step">
                    <div class="col col-xs-9 pd0">
                        <label for="" class="label">URL</label>
                        <input type="text" class="input" name="url" id="url_site" onchange="getEncoding()">
                    </div>
                    <div class="col col-xs-3 pd0">
                        <label for="" class="label"><?php echo $this->t('Кодировка');?></label>
                        <input type="text" class="input" name="charset_site" id="encoding" >
                    </div>
                </div>
            </div>
            <div class="second_step step hidden">
                <div id="manual_info" class="install_second_step">
                    <div class="col">
                        <label for="" class="label"><?php echo $this->t('Кэширование');?></label>
                        <?php
                        $pdoIsActive = (defined('PDO::ATTR_DRIVER_NAME'));
                        $mysqliIsAvailable = (function_exists('mysqli_connect'));
                        $sqlite3IsAvailable = (class_exists('Sqlite3'));
                        ?>
                        <select name="cache_adapter"  id="chache" class="magic_select">
                            <option value="File"><?php echo $this->t('Файлы');?></option>
                            <?php if ($mysqliIsAvailable) { ?>
                                <option value="MysqlMysqli">MySQL</option>
                            <?php } ?>

                            <?php if ($sqlite3IsAvailable) { ?>
                                <option value="Sqlite3">Sqlite3</option>
                            <?php } ?>

                            <?php if ($pdoIsActive) {
                                $mysqlPDOIsAvailable = (in_array('mysql', PDO::getAvailableDrivers()));
                                $sqlitePDOIsAvailable = (in_array('sqlite', PDO::getAvailableDrivers()));
                                ?>

                                <?php if ($mysqlPDOIsAvailable and !$mysqliIsAvailable) { ?>
                                    <option value="Mysql">MySQL</option>
                                <?php } ?>

                                <?php if ($sqlitePDOIsAvailable and !$sqlite3IsAvailable) { ?>
                                    <option value="Sqlite">Sqlite</option>
                                <?php }
                            } ?>
                            <option value="Not"  selected><?php echo $this->t('Не кэшировать');?></option>
                        </select>
                    </div>
                    <div class="mysql">
                        <div class="col">
                            <label for="" class="label"><?php echo $this->t('Хост');?></label>
                            <input type="text" class="input"
                                   id="host"
                                   placeholder="localhost"
                                   name="host"
                                   value="<?php echo OtherFunctions::returnIfIsset(@Settings::staticGet('dbHost'), 'localhost');?>">
                        </div>
                        <div class="col">
                            <label for="" class="label"><?php echo $this->t('База данных');?></label>
                            <input type="text"
                                   data-required="true"
                                   class="input"
                                   id="dbname"
                                   name="dbname"
                                   placeholder="DB Name"
                                   value="<?php echo OtherFunctions::returnIfIsset(@Settings::staticGet('dbName'), 'dolly');?>">
                        </div>
                        <div class="col">
                            <label for="" class="label"><?php echo $this->t('Пользователь');?></label>
                            <input type="text"
                                   data-required="true"
                                   class="input"
                                   id="username"
                                   name="user"
                                   placeholder="DB User"
                                   value="<?php echo OtherFunctions::returnIfIsset(@Settings::staticGet('dbUser'), 'root');?>">
                        </div>
                        <div class="col">
                            <label for="" class="label"><?php echo $this->t('Пароль');?></label>
                            <input type="text"
                                   data-required="true"
                                   class="input"
                                   id="dbpassword"
                                   name="password"
                                   placeholder="DB Password"
                                   value="<?php echo OtherFunctions::returnIfIsset(@Settings::staticGet('dbPassword'), '');?>">
                        </div>
                    </div>
                    <!--
                    <div class="files">
                        <div class="col">
                            <label for="" class="label">Имена файлов</label>
                            <select name="file_names" id="file_names" class="magic_select">
                                <option value="Полное имя">Полное имя</option>
                                <option value="Хэш">Хэш</option>
                            </select>
                        </div>
                    </div>
                    -->
                    <div class="chaching_files">
                        <div class="col">
                            <label for="" class="label"><?php echo $this->t('Кэширование файлов с других доменов');?></label>
                            <!--
                            <div class="check">
                                <input type="checkbox" class="super_checkbox" id="js_check">
                                <label for="js_check" class="checkbox_label">Подключаемые JS</label>
                            </div>-->
                            <div class="check">
                                <input type="checkbox" class="super_checkbox cb2" value="true"  name="css" id="css_check">
                                <label for="css_check" class="checkbox_label"><?php echo $this->t('Подключаемые JS/CSS');?></label>
                            </div>
                            <div class="check">
                                <input type="checkbox" class="super_checkbox cb2" value="true" name="img" id="img_check">
                                <label for="img_check" class="checkbox_label"><?php echo $this->t('Изображения');?></label>
                            </div>
                        </div>
                        <div class="col">
                            <select name="cache_limit_type" id="chaching_all" class="magic_select">
                                <option value="notCache"><?php echo $this->t('Кэшировать всё, кроме');?></option>
                                <option value="cacheOnly"><?php echo $this->t('Кешировать только');?></option>
                            </select>
                            <textarea name="not_cached" id="chache_execlude" class="input"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="third_step step last_step hidden">
                <div class="success_text">

                    <span class="success_text_1"><?php echo $this->t('Подождите...');?></span>
                    <span class="success_text_2" style="display:none;"><?php echo $this->t('Установка успешно<br>завершена!');?></span>
                </div>
            </div>
        </div>

        <div class="install_navigation">
            <div class="left col-xs-6 pd0">
                <div class="back button" style="display: none;">
                    <?php echo $this->t('Назад');?>
                </div>
            </div>
            <div class="right col-xs-6 pd0">
                <div class="next button" id="next_button">
                    <?php echo $this->t('Далее');?>

                </div>
            </div>
        </div>
    </form>
    <footer id="install_footer">
        <div class="col-xs-6 pd0">
            <div class="languages_switcher">
                <div class="current <?php echo (@Settings::staticGet('language')) ? @Settings::staticGet('language') : 'ru';?>"> <?php
                    $langs = array ('ru' => 'Русский',
                                    'en' => 'English');
                    echo $langs[(@Settings::staticGet('language')) ? @Settings::staticGet('language') : 'ru'];?>
                </div>
                <div class="languages_list">
                    <ul class="list">
                        <li><a href="/index.php?act_dolly=lang&lang=en">English</a></li>
                        <li><a href="/index.php?act_dolly=lang&lang=ru">Русский</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-6 pd0">
            <div class="copyright">
                <a href="https://dollysites.com/">dollysites.com</a>
            </div>
        </div>
    </footer>
    </form>
</div>
<script type="text/javascript">
    var NOT_SITE_URL = '<?php echo $this->t('Введите url сайта!');?>'
</script>
<script src="dolly_templates/js/jquery-1.12.2.min.js"></script>
<script src="dolly_templates/js/install_navigation.js"></script>
<script src="dolly_templates/js/magic_select.js"></script>
<script type="text/javascript">
    function getEncoding() {
        //$('#encoding_flag').hide();

        $.ajax({
            url  : 'index.php?act_dolly=get_encoding',
            type : 'post',
            data : ({
                url : $('#url_site').val()
            }),
            success : function(encoding) {

                $('#encoding').val(encoding);
                if (!encoding) {
                    $('#encoding').val('');
                    $('#encoding_flag').hide();

                } else {
                    $('#encoding').removeAttr("disabled");
                    $('#encoding_flag').show();


                }
            },
            done : function(e) {
                $('.bot_wrap').show();
                stopLoadingAnimation()
            }
        });

    };

    $('#form1').submit(function() {

        return true;
    })

    $(document).ready(function() {
        $('#next_button').on('click', function () {

            return true
        })

        $('#mysql_settings').hide()
        $('#notCache').show();

        $('#cache_adapter').change(function() {
            $('#mysql_settings').hide();
            $('#notCache').show();

            if ($('#cache_adapter').val() == 'Mysql' || $('#cache_adapter').val() == 'MysqlMysqli') {
                $('#mysql_settings').show();
            }

            if ($('#cache_adapter').val() == 'Not') {
                $('#notCache').hide();
            }
        })
        $('#language').change(function() {
            document.location.href="/index.php?act_dolly=lang&lang=" + $('#language').val();
        })
    })
</script>
<script>
    jQuery(function($) {

        $('.languages_switcher .current').click(function() {
            $(this).parent().find('.languages_list').slideToggle(300);
            $(this).parent().toggleClass('open');
        })
        $('.magic_select').magicselect();
        $('input#url').focus(function() {
            if ($(this).val() == '') {
                $(this).attr('placeholder', '');
                $(this).val('http://');
            }
        });
        $('input#url').blur(function() {
            if ($(this).val() == '' || $(this).val() == 'http://') {
                $(this).attr('placeholder', 'http://');
                $(this).val('');
            }
        })

        $(document).on('change', 'select#chache', function() {
            if ($(this).val() == 'Mysql' || $(this).val() == 'MysqlMysqli') {
                $('.mysql').show();
                $('.files').hide();
                $('.chaching_files').show();
            }
            if ($(this).val() == 'Sqlite' || $(this).val() == 'Sqlite3') {
                $('.mysql').hide();
                $('.files').hide();
                $('.chaching_files').show();
            }
            if ($(this).val() == 'File') {
                $('.mysql').hide();
                $('.files').show();
                $('.chaching_files').show();
            }
            if ($(this).val() == 'Not') {
                $('.chaching_files').hide();
                $('.mysql').hide();
                $('.files').hide();
            }
        })
        $('select#chache').val('File');
        $('select#chache').trigger('change');
    })
</script>
<dv id="info_block" style="display: none; top: 20px; right: 20px; width: 550px; height: 75px;background-color: white;border: 2px solid #569fd0;z-index: 9999;position: absolute;color: #569fd0;padding: 15px;font-size: 16px;"><?php echo $this->t('Установка завершена. Теперь можно <a href="?dollyeditor">отредактировать эту страницу</a>, или <a href="/admin.php">перейти в админку'); ?></a></dv>
</body>
</html>