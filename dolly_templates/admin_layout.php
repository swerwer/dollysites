<?php
function _select(Array $options, $selected = '')
{
    $tempArr = array();
    foreach ($options as $key => $option) {
        if ($selected == $option[0]) {
            unset($options[$key]);
            array_unshift($options, $option);
        } else {
            $tempArr[$key + 1] = $option;
        }
    }
    foreach ($options as $option) {
        $selectedStr = ($selected == $option[0]) ? 'selected' : '';
        echo "<option value='{$option[0]}' {$selectedStr}>{$option[1]}</option>";
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Dolly Sites</title>

    <link rel="stylesheet" href="dolly_templates/css/ztree.css">
    <link rel="stylesheet" media="all" href="dolly_templates/css/jquery.fancybox.css">
    <link rel="stylesheet" href="dolly_css/bootstrap.min.css">
    <link href="dolly_templates/css/jquery.formstyler.css" rel="stylesheet" />
    <link rel="stylesheet" href="dolly_templates/css/main.css">
    <link rel="stylesheet" href="dolly_templates/js/dist/themes/default/style.min.css">

    <meta charset="utf-8">
    <meta name="robots" content="noindex"/>

</head>
<?php $version = $this->getVersion();
$needMessage = false;

if (is_array($version)) {
    $needMessage = true;
}
?>

<body <?php echo ($needMessage) ? 'style="overflow: hidden"' : '';?>>

<?php if ($needMessage):?>
    <div id="blockpage" style="width: 100%; height: 100%; background-color: black; opacity: .7; z-index: 150; position: absolute;"></div>
    <?php if ($version['slots'] !== 0):?>
        <div id="secure_info" style="display: block; font-size: 16px; padding: 20px; position: absolute; width: 460px; height: 160px; z-index: 160; top: 300px; left: 50%; margin-left: -230px; background-color: white; border-radius: 15px; border: 1px solid red;">
            Вы первый раз запускаете скрипт с айпи <i><?php echo $version['ip'];?></i><br>
            Чтобы продолжить работать, нужно привязать скрипт к этому адресу.
            У вас <?php echo $version['slots'];?> свободных слотов из <?php echo $version['max_slots'];?> ,
            <a target='_blank' href="/admin.php?action=add_ip_to_wl&ip=<?php echo $version['ip'];?>" onclick="return confirm('are you sure?');">привязать</a>?<br><br>
            <a target='_blank' href="<?php echo $version['url_base'];?>">Управление привязкой</a>
        </div>
    <?php else: ?>
        <div id="secure_info" style="font-size: 16px; padding: 20px; position: absolute; width: 460px; height: 175px; z-index: 160; top: 300px; left: 50%; margin-left: -230px; background-color: white; border-radius: 15px; border: 1px solid red;">
            Вы первый раз запускаете скрипт с айпи <i><?php echo $version['ip'];?></i><br>
            Чтобы продолжить работать, нужно привязать скрипт к этому адресу, но свободных слотов у вас нет. Можете удалить ненужный, либо докупить:<br><br>
            <a target='_blank' href="<?php echo $version['url_base'];?>" style="top:20px">Управление привязкой</a>
        </div>
    <?php endif;?>
<?php endif;?>
<div class="content">
    <header>
        <div class="top">
            <div class="container">
                <div class="col-xs-3 pd0">
                    <div class="logo">DollySites <span style="font-size:10px;color:#7b7c7d;">v.<?php echo Constants::VERSION;?></span>
                    </div>

                    <div class="languages_switcher">
                        <a href = "/admin.php?action=lang&lang=en"><img src="dolly_templates/images/en.png"></a>
                        <a href = "/admin.php?action=lang&lang=ru" style="padding-left:3px;"><img src="dolly_templates/images/ru.png"></a>
                    </div>
                </div>
                <div class="account col-xs-5 right pd0">
                    <div class="logout"><a href="/admin.php?action=logout"><?php echo $this->t('Выход'); ?></a></div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="left-side col-xs-3 pd0 nofloat">
            <div class="main_nav">
                <ul class="list">
                    <?php
                    $menu = array(
                        array('action' => 'editor',
                              'text' => $this->t('Визуальный редактор')),
                        array('action' => 'forms_handler',
                              'text' => $this->t('Обработчик форм')),
                        array('action' => 'scripts',
                              'text' => $this->t('Скрипты')),
                        array('action' => 'replaces',
                              'text' => $this->t('Замены в тексте')),
                        array('action' => 'content',
                              'text' => $this->t('Контент')),
                        array('action' => 'images',
                              'text' => $this->t('Изображения')),
                        array('action' => 'auth_info',
                              'text' => $this->t('Настройки авторизации')),
                    );
                    foreach ($menu as $item): ?>
                        <li <?php echo ($action == $item['action']) ? 'class = "active"' : ''; ?>>

                            <a href="?action=admin_<?php echo $item['action']; ?>"><?php echo $item['text']; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php
            $plugins = @json_decode(file_get_contents('plugins.json'));
            if ($plugins): ?>
            <div class="main_nav">
                <h4><?php echo $this->t('Плагины');?></h4>
                <ul class="list">
                    <?php

                    foreach ($plugins as $item): ?>
                        <li <?php echo ($action  == 'plugins' and $_GET['name'] == $item->name) ? 'class = "active"' : ''; ?>>

                            <a href="?action=plugins&name=<?php echo $item->name; ?>"><?php echo $item->title; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif;?>

            <div class="second-nav">
                <ul class="list">
                    <?php if (Settings::staticGet('cacheBackend') == 'File') {?>
                        <li><a href="#" id="get_archive"><?php echo $this->t('Выгрузить архив'); ?></a></li>
                    <?php } ?>
                    <li class="important"><a href="#" id="dolly_clear_cache"><?php echo $this->t('Очистить кэш'); ?></a></li>
                    <li class="important"><a href="#" id="dolly_remove"><?php echo $this->t('Удалить сайт'); ?></a></li>
                </ul>
            </div>
            <div class="third-nav">
                <ul class="list">
                    <?php $url = Settings::staticGet('donor_url');

                    $domain = parse_url($url, PHP_URL_HOST);?>
                    <li class="important">
                        <?php echo $this->t('Источник');?>: <span>
                            <a target="_blank" href="<?php echo $url;?>"><?php echo $domain;?></a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="right-side col-xs-9 pd0 nofloat">

            <?php include "dolly_templates/{$action}.php";  ?>

        </div>
    </div>
</div>
<div class="notifications_window">
</div>
<div id="file_chose_window" class="window">
    <div class="topbar">
        <div class="window_title"><?php echo $this->t('Выбор файла для редактирования'); ?></div>
        <div class="close"><i class="icon fa-close"></i></div>
    </div>
    <div class="file_window_content">
        <div id="jstree" style="padding-left: 15px;
  padding-top: 15px;
  overflow: auto;
  width: 650px;
  height: 330px;"></div>
        <div class="bottom">
            <div class="col-xs-8 pd0 file_bottom hidden">
                <span class="filename"><?php echo $this->t('Выбран файл'); ?></span>
                <div class="file_link">//</div>
            </div>
            <div class="col-xs-4 pd0 right">
                <div class="chose_file button right"
                     id="select_file_button"><?php echo $this->t('Выбрать файл'); ?></div>
            </div>
        </div>
    </div>
</div>
<script>
    var HANDLER_SUCCESS_TEXT = '<?php echo $this->t('Обработчик успешно установлен');?>'
</script>
<script src="dolly_templates/js/jquery-1.12.2.min.js"></script>

<script src="dolly_templates/js/jquery.fancybox.js"></script>

<script src="dolly_templates/js/files.js"></script>
<script src="dolly_templates/js/magic_select.js"></script>
<script src="dolly_templates/js/main.js"></script>
<script src="dolly_templates/js/dist/jstree.js"></script>
<script src="../dolly_js/tinymce/tinymce.min.js"></script>

<script type="text/javascript">

    var h = screen.height - 350;
    tinymce.init({
        selector: "#input",
        extended_valid_elements: 'head, body, style, link, script, html',
        plugins: [
            "jbimages advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks",
            "insertdatetime media table contextmenu paste moxiemanager",
            "fullpage"
        ],

        valid_elements: '*[*]',
        convert_urls: false,
        height: h,
        cleanup: false,
        verify_html: false,
        valid_styles: false,
        mode: "textareas",
        language: '<?php echo (Settings::staticGet('language') == 'ru') ? 'ru' : 'en';?>',
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages"
    });




</script>
<script>
    // scripts part of jquery code
    jQuery(function ($) {
        $('#jstree').jstree({
            'core': {'data' : { 'url' : function (node) {
                return node.id === '#' ?
                    '/admin.php?action=get_pages' :
                    '/admin.php?action=get_pages&parent=' + node.id;
            }}
            }
        }).on("changed.jstree", function (e, data) {
            if (data.selected.toString().indexOf('.htm') > 0) {
                $('.bottom .file_link').html(data.selected);
                $('.file_field').html(data.selected);
                $('.bottom .file_bottom').show();
            } else {
                $('#jstree').jstree('open_node', data.selected.toString());
            }

        });

        document.source_url = $('.third-nav span a').html();
        if(document.source_url.length > 25) {
            $('.third-nav span a').html(document.source_url.slice(0, 25) + '...');
        }
        $('.table_content .item').dblclick(function () {
            $(this).find('.textarea_hidden').toggle();
            $(this).find('.hidden_buttons').toggle();
            $(this).find('.edit').toggle();
            $(this).toggleClass('active');
        });
        $('.table_content .item .edit').click(function () {
            $('.item').find('.textarea_hidden').hide();
            $('.item').find('.hidden_buttons').hide();
            $('.item').find('.edit').show();

            $(this).parent().parent().trigger('dblclick');

        })
        $('.table_content .item .remove').click(function () {
            $(this).parent().parent().parent().remove();
        });
        $('.table_content .item .save').click(function () {
            var code = $(this).parent().parent().parent().find('textarea').val();
            $(this).parent().parent().parent().find('.shortcode .code').text(code);
            $(this).parent().parent().parent().trigger('dblclick');
        });
        $('.table_content .item').each(function () {
            var code = $(this).find('textarea').val();
            $(this).find('.shortcode .code').text(code);
        })
    })
</script>
</body>
</html>