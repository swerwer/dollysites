<div class="topbar">
    <h1 class="main-title"><?php echo $this->t('Обработчик форм');?></h1>

    <!--<div class="right button save">
        Сохранить
    </div>-->
</div>
<div class="forms inner">
    <div class="tabs">
        <div class="tabs_selectors">
            <ul class="list">
                <li id="tab_0"><?php echo $this->t('Добавить');?></li>
                <!--<li id="tab_1">Управление обработчиками</li>-->
                <li id="tab_2"><?php echo $this->t('Заказы');?></li>
            </ul>
        </div>
        <div class="tabs_content">
            <div class="tab tab_0">
                <div class="add_window">
                    <div class="chose_file">
                        <div class="col-xs-4 pd0">
                            <label for="file"><?php echo $this->t('Выберите файл');?></label>
                        </div>
                        <div class="col-xs-8 right pd0">
                            <input type="file" class="hidden">
                            <div class="col-xs-9">
                                <div class="file_field">
                                    <?php echo $this->t('выберите файл');?>

                                </div>
                            </div>
                            <div class="col-xs-3 right pd0">
                                <div class="button open_file">
                                    <?php echo $this->t('Выбрать');?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="forms_container"></div>
                </div>
            </div>

            <div class="tab tab_1">
                
            </div>
            <div class="tab tab_2">
                <div class="orders">
                    <div class="orders_list">
                        <div class="thead">
                            <div class="col col-xs-5 pd0">
                                <span class="label"><?php echo $this->t('Текст');?></span>
                            </div>
                            <div class="col-xs-2 pd0 col">
                                <span class="label"><?php echo $this->t('Дата');?></span>
                            </div>
                            <div class="col-xs-5 pd0 col">
                                <span class="label"><?php echo $this->t('Действие');?></span>
                            </div>
                        </div>
                        <div class="tbody">
<?php if ($orders) {
    foreach ($orders as $key => $order) {
        if (@trim($order['content'])):
            ?>
            <div class="item">
                <div class="col-xs-5 pd0">
                    <label for="checkbox[1]">
                        <?php echo str_replace("\n", '<br>', $order['content']); ?>

                    </label>
                </div>
                <div class="col col-xs-2 pd0">
                    <?php echo $order['date']; ?>
                </div>
                <div class="col col-xs-5 pd0">
                    <a href="/admin.php?action=del_order&id=<?php echo $key; ?>"><?php echo $this->t('Удалить');?></a>
                </div>
            </div>
            <?php
        endif;
    }
}
 ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="dolly_templates/js/jquery-1.12.2.min.js"></script>

<script src="dolly_templates/js/jquery.fancybox.js"></script>

<script src="dolly_js/bootstrap.min.js"></script>
