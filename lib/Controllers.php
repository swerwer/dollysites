<?php
require 'Parser.php';
require 'simple_html_dom.php';
require 'HttpClient.php';
require 'UITranslate.php';
require 'Settings.php';
error_reporting(E_ALL);
if (!defined('PHP_VERSION_ID')) {
    $version = explode('.', PHP_VERSION);
    define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}

abstract class Controllers
{
    public static $VERSION = 1.2;
    const DEFAULT_CHARSET = 'utf-8';
    const API_URL = 'https://dollysites.com/system/api.php';
    protected $_parser;
    protected $_dom;
    protected $_t;
    public $_settings;
    protected $_api;

    public function __construct()
    {
        $settings = new Settings();

        $dom = new simple_html_dom();
        $parser = new Parser($dom, new HttpClient());

        UITranslate::setLanguage(Settings::staticGet('language'));
        $translate = UITranslate::getInstance();


        $this->_dom = $dom;
        $this->_parser = $parser;
        $this->_t = $translate;
        $this->_settings = $settings;
        $this->_api = new ServerApiClient();
    }

    public function t($word)
    {
        return $this->_t->t($word);
    }

    public static function getApiKey()
    {
        return '{API_KEY}';
    }

    public static function setCharset($charset = null)
    {
        if (!$charset) {
            $charset = OtherFunctions::returnIfIsset($charset, self::DEFAULT_CHARSET);
        }
        header('Content-Type: text/html; charset=' . $charset, true);
    }


    protected function _auth($user, $password, $to = null, $redirect = true)
    {
        $authData = explode('|', trim(file_get_contents('login.ini')));
        if ($user == $authData[0] AND $password == $authData[1]) {
            $this->_setAuthCookies($authData);
            if ($redirect) {
                Controllers::redirect(OtherFunctions::returnIfIsset($to, '/admin.php'));
            }
            return true;
        }
        return false;
    }

    /**
     * @param $authData
     */
    protected function _setAuthCookies($authData)
    {
        $hour = 3600;
        $day = $hour * 24;
        $year = $day * 364;

        $setCookieTime = $year * 2;
        setcookie('auth', md5($authData[0] . $_SERVER['HTTP_HOST']), time() + $setCookieTime);
    }

    public function isAdmin()
    {
        $authData = explode('|', trim(file_get_contents('login.ini')));
        return (isset($_COOKIE['auth']) AND $_COOKIE['auth'] == md5($authData[0] . $_SERVER['HTTP_HOST']));
    }

    public static function redirect($to)
    {
        header("Location: {$to}", true);
    }

    public function returnSavedPage($page = null, $echo = true, $iconv = false)
    {
        if (!$page) {
            $page = "./{$this->_parser->cacheDir()}/index.html";
        } else {
            $page = urldecode($page) . (($this->_parser->equals->needAddHtmlToFileName('text/html', $page)) ? '.html' : '');
        }
        $return = CacheBackend::getFile($page);
        if (!$echo) {
            return $return;
        }
        echo $return;
    }

    public function saveCacheSettings()
    {
        $settings = new Settings();

        $settings->set('dbHost',     $_POST['host']);
        $settings->set('dbName',     $_POST['dbname']);
        $settings->set('dbUser',     $_POST['user']);
        $settings->set('dbPassword', $_POST['password']);

        $settings->save();

        @file_put_contents('notCacheUrls', $_POST['not_cached']);

        return $settings;
    }


}