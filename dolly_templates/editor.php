<form method="post" id="form"  action="../admin.php?action=save_page" onsubmit="tinyMCE.triggerSave(true, true);">

<div class="topbar">
    <h1 class="main-title"><?php echo $this->t('Визуальный редактор');?></h1>
    <div class="right button save" action="../admin.php?action=save_page">
        <?php echo $this->t('Сохранить');?>

    </div>
</div>

<div class="visual_editor_box inner">
    <div class="chose_file">
        <div class="col-xs-4 pd0">
            <label for="file"><?php echo $this->t('Выберите файл');?></label>
        </div>
        <div class="col-xs-8 right pd0">
            <input type="file" class="hidden">
            <div class="col-xs-9">
                <div class="file_field">
                    <?php echo $this->t('выберите файл');?>
                </div>
            </div>
            <div class="col-xs-3 right pd0">
                <div class="button open_file">
                    <?php echo $this->t('Выбрать');?>
                </div>
            </div>
        </div>
    </div>
    <div class="editor">
        <textarea id = "input" name="page"></textarea>
        <input type="hidden" name = "path" id = "path">

    </div>
</div>
    </form>

<script src="dolly_templates/js/jquery-1.12.2.min.js"></script>
<script src="dolly_templates/js/jquery.fancybox.js"></script>

<script src="dolly_templates/js/magic_select.js"></script>
<script src="dolly_templates/js/main.js"></script>
<script src="../dolly_js/tinymce/tinymce.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

    $(document).bind('DOMNodeInserted', function(e) {
        if($(e.target).attr('id') == 'mceu_16') {
            $('body').append('<div id="editortop" class="' + $('.right-side').css('width')+ '"></div>');
            $('#editortop').css({position: 'absolute',
                cursor: 'text',
                left: $('#mceu_16').offset().left,
                top: $('#mceu_16').offset().top,
                width: $('#mceu_16').css('width'),
                height: $('#mceu_16').css('height')});
            $('#editortop').click(function() {
                $(this).css({display: 'none'});
                $('.right-side').animate({width: '100%'});
            });

            $(document).click(function(event) {
                var id = $(event.target).attr('id');
                if(id == undefined)
                    id = '';

                if(id == 'editortop' || $('.right-side').css('width') == $('#editortop').attr('class') || $(event.target).closest('.editor').length || $(event.target).closest('.mce-panel').length || id.match(/mceu_/) == 'mceu_')
                    return;

                $('.right-side').css({width: '79%'});
                $('#editortop').css({display: 'block'});
            })
        }
    });
    })

    var h = screen.height - 350;
    tinymce.init({
        selector: "#input",
        extended_valid_elements : 'head, body, style, link, script, html',
        plugins: [
            "jbimages advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks",
            "code fullscreen",
            "insertdatetime media table contextmenu paste moxiemanager",
            "fullpage"
        ],

        valid_elements : '*[*]',
        convert_urls : false,
        height : h,
        cleanup : false,
        //relative_urls: false,
        verify_html : false,
        valid_styles : false,
        mode : "textareas",
        language : '<?php echo (Settings::staticGet('language') == 'ru') ? 'ru': 'en';?>',
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages"
    });


</script>
<script src="../dolly_js/main.js"></script>
