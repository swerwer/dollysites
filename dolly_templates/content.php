<?php
function upFirstLetter($str, $encoding = 'UTF-8')
{
    return mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding)
        . mb_substr($str, 1, null, $encoding);
}

$target = array(
    array('en', $this->t('Английский')),
    array('ru', $this->t('Русский'))
);

$source = array(
    array('ru', $this->t('Русский')),
    array('en', $this->t('English')),
);

$options = array(
    array('az', upFirstLetter($this->t('азербайджанский'))),
    array('sq', upFirstLetter($this->t('албанский'))),
    array('am', upFirstLetter($this->t('амхарский'))),
    array('ar', upFirstLetter($this->t('арабский'))),
    array('hy', upFirstLetter($this->t('армянский'))),
    array('af', upFirstLetter($this->t('африкаанс'))),
    array('eu', upFirstLetter($this->t('баскский'))),
    array('ba', upFirstLetter($this->t('башкирский'))),
    array('be', upFirstLetter($this->t('белорусский'))),
    array('bn', upFirstLetter($this->t('бенгальский'))),
    array('bg', upFirstLetter($this->t('болгарский'))),
    array('bs', upFirstLetter($this->t('боснийский'))),
    array('cy', upFirstLetter($this->t('валлийский'))),
    array('hu', upFirstLetter($this->t('венгерский'))),
    array('vi', upFirstLetter($this->t('вьетнамский'))),
    array('ht', upFirstLetter($this->t('гаитянский (креольский)'))),
    array('gl', upFirstLetter($this->t('галисийский'))),
    array('nl', upFirstLetter($this->t('голландский'))),
    array('mrj', upFirstLetter($this->t('горномарийский'))),
    array('el', upFirstLetter($this->t('греческий'))),
    array('ka', upFirstLetter($this->t('грузинский'))),
    array('gu', upFirstLetter($this->t('гуджарати'))),
    array('da', upFirstLetter($this->t('датский'))),
    array('he', upFirstLetter($this->t('иврит'))),
    array('yi', upFirstLetter($this->t('идиш'))),
    array('id', upFirstLetter($this->t('индонезийский'))),
    array('ga', upFirstLetter($this->t('ирландский'))),
    array('it', upFirstLetter($this->t('итальянский'))),
    array('is', upFirstLetter($this->t('исландский'))),
    array('es', upFirstLetter($this->t('испанский'))),
    array('kk', upFirstLetter($this->t('казахский'))),
    array('kn', upFirstLetter($this->t('каннада'))),
    array('ca', upFirstLetter($this->t('каталанский'))),
    array('ky', upFirstLetter($this->t('киргизский'))),
    array('zh', upFirstLetter($this->t('китайский'))),
    array('ko', upFirstLetter($this->t('корейский'))),
    array('xh', upFirstLetter($this->t('коса'))),
    array('la', upFirstLetter($this->t('латынь'))),
    array('lv', upFirstLetter($this->t('латышский'))),
    array('lt', upFirstLetter($this->t('литовский'))),
    array('mg', upFirstLetter($this->t('малагасийский'))),
    array('ms', upFirstLetter($this->t('малайский'))),
    array('ml', upFirstLetter($this->t('малаялам'))),
    array('mt', upFirstLetter($this->t('мальтийский'))),
    array('mk', upFirstLetter($this->t('македонский'))),
    array('mi', upFirstLetter($this->t('маори'))),
    array('mr', upFirstLetter($this->t('маратхи'))),
    array('mhr', upFirstLetter($this->t('марийский'))),
    array('mn', upFirstLetter($this->t('монгольский'))),
    array('de', upFirstLetter($this->t('немецкий'))),
    array('ne', upFirstLetter($this->t('непальский'))),
    array('no', upFirstLetter($this->t('норвежский'))),
    array('pa', upFirstLetter($this->t('панджаби'))),
    array('pap', upFirstLetter($this->t('папьяменто'))),
    array('fa', upFirstLetter($this->t('персидский'))),
    array('pl', upFirstLetter($this->t('польский'))),
    array('pt', upFirstLetter($this->t('португальский'))),
    array('ro', upFirstLetter($this->t('румынский'))),
    array('ceb', upFirstLetter($this->t('себуанский'))),
    array('sr', upFirstLetter($this->t('сербский'))),
    array('si', upFirstLetter($this->t('сингальский'))),
    array('sk', upFirstLetter($this->t('словацкий'))),
    array('sl', upFirstLetter($this->t('словенский'))),
    array('sw', upFirstLetter($this->t('суахили'))),
    array('su', upFirstLetter($this->t('сунданский'))),
    array('tg', upFirstLetter($this->t('таджикский'))),
    array('th', upFirstLetter($this->t('тайский'))),
    array('tl', upFirstLetter($this->t('тагальский'))),
    array('ta', upFirstLetter($this->t('тамильский'))),
    array('tt', upFirstLetter($this->t('татарский'))),
    array('te', upFirstLetter($this->t('телугу'))),
    array('tr', upFirstLetter($this->t('турецкий'))),
    array('udm', upFirstLetter($this->t('удмуртский'))),
    array('uz', upFirstLetter($this->t('узбекский'))),
    array('uk', upFirstLetter($this->t('украинский'))),
    array('ur', upFirstLetter($this->t('урду'))),
    array('fi', upFirstLetter($this->t('финский'))),
    array('fr', upFirstLetter($this->t('французский'))),
    array('hi', upFirstLetter($this->t('хинди'))),
    array('hr', upFirstLetter($this->t('хорватский'))),
    array('cs', upFirstLetter($this->t('чешский'))),
    array('sv', upFirstLetter($this->t('шведский'))),
    array('gd', upFirstLetter($this->t('шотландский'))),
    array('et', upFirstLetter($this->t('эстонский'))),
    array('eo', upFirstLetter($this->t('эсперанто'))),
    array('jv', upFirstLetter($this->t('яванский'))),
    array('ja', upFirstLetter($this->t('японский'))));

?>


<script type="text/javascript">
    function setAction(action) {
        $('#form').attr("action", '../admin.php?action=' + action)
    }

</script>
<form method="post" enctype="multipart/form-data" id="form" action="../admin.php?action=save_page">

    <div class="topbar">

        <h1 class="main-title"><?php echo $this->t('Контент'); ?></h1>

        <div class="right button save">
            <?php echo $this->t('Сохранить'); ?>
        </div>
    </div>

    <div class="content_editor inner">
        <div class="tabs">
            <div class="tabs_selectors">
                <ul class="list">
                    <li id="tab_0" onclick="setAction('translate_save')"><?php echo $this->t('Переводчик'); ?></li>
                    <li id="tab_1" onclick="setAction('synonims_save')"><?php echo $this->t('Синонимайзер'); ?></li>
                    <li id="tab_2"
                        onclick="setAction('save_content_settings')"><?php echo $this->t('Настройки'); ?></li>
                </ul>
            </div>
            <div class="tabs_content">
                <div class="tab tab_0">
                    <div class="tanslator_form">
                        <div class="col-xs-12 pd0 item">
                            <!--<div class="topper">
                                <span class="label"><?php echo $this->t('Выберите переводчик'); ?></span>
                            </div>-->
                            <div class="clear"></div>
                            <div class="sinonymizer">
                                <input type="checkbox"
                                       id="GoogleTranslate"
                                       class="super_checkbox"
                                       name="translate_backend"
                                       value="YandexTranslate"
                                    <?php echo (@Settings::staticGet('translateAdapter') and @Settings::staticGet('translateAdapter') !== 'NotTranslate') ? 'checked' : ''; ?>
                                >
                                <label for="GoogleTranslate" class="label syn">
                                    <?php echo $this->t('Включить переводчик'); ?>
                                </label><br>
                            </div>
                        </div>


                        <div class="col-xs-12 pd0 item translaters">
                            <div class="disabled"></div>
                            <!--
                            <select name="translate_backend" class="magic_select">
                                <!--<option value="1">Яндекс переводчик</option>-->
                            <?php
                            $translaters = array(array('GoogleTranslate',
                                'Google ' . $this->t('переводчик')),
                                array('YandexTranslate',
                                    'Yandex ' . $this->t('переводчик')),
                                array('BaiduTranslate',
                                    'Baidu ' . $this->t('переводчик')),

                            );
                            //_select($translaters, Settings::staticGet('translateAdapter')); ?>
                            <!--</select>-->
                            <table class="table borderless " border="0">
                                <tr>

                                    <div class="topper">
                                        <td><span class="label"><?php echo $this->t('Язык сайта'); ?></span></td>
                                        <td><span class="label"><?php echo $this->t('Перевести на язык'); ?></span></td>
                                    </div>
                                </tr>
                                <tr>
                                    <div class="first">
                                        <td>
                                            <select name="translate_source" class="magic_select">
                                                <?php
                                                $optionsSource = array_merge($source, $options);

                                                _select($optionsSource, @Settings::staticGet('translateSource'));
                                                ?>
                                            </select></td>
                                    </div>
                                    <div class="change_position">
                                        <!-- <div class="btn">
                                             <i class="fa fa-angle-left"></i>
                                             <i class="fa fa-angle-right"></i>
                                         </div>-->

                                    </div>
                                    <div class="second">
                                        <td>
                                            <select name="translate_target" class="magic_select">
                                                <?php
                                                $optionsTarget = array_merge($target, $options);
                                                _select($optionsTarget, @Settings::staticGet('translateTarget'));
                                                ?>
                                            </select>
                                        </td>
                                    </div>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab tab_1">
                    <div class="sinonymizer">

                        <div class="">
                            <input type="checkbox"
                                   id="status"
                                   class="super_checkbox"
                                   name="synonims_status"
                                   value="on"
                                <?php echo (Settings::staticGet('synonimize')) ? 'checked' : ''; ?>
                            >
                            <label for="status" class="label syn">
                                <?php echo $this->t('Включить синонимайзер'); ?>
                            </label>
                        </div>
                        <div class="options">
                            <div class="disabled"></div>
                            <div class="left pd0">
                                <span class="label"><?php echo $this->t('Выберите базу'); ?></span>
                            </div>
                            <div class="right pd0">
                                <select name="synonims_file" class="magic_select">
                                    <option value="none"><?php echo $this->t('Выберите файл синонимов'); ?></option>
                                    <?php
                                    $options = array();
                                    foreach (glob('*.syns') as $dict) {
                                        $options[] = array($dict, $dict);
                                    }
                                    _select($options, @Settings::staticGet('synsDictonary'));
                                    ?>
                                </select>
                            </div>
</form>

<div class="col-xs-12 pd0 devider">
    <span class="label grey"><?php echo $this->t('ИЛИ'); ?></span>
</div>
<div class="left pd0"><span class="label"><?php echo $this->t('Загрузите файл'); ?></span></div>
<div class="right pd0">
    <input type="file" id="upload_files" name="upload_files">
    <label for="upload_files"><?php echo $this->t('Нажмите для выбора файла'); ?></label>
    <!--<div class="button upload_button">Загрузить</div>-->
</div>
<div class="left pd0">
    <span class="label"><?php echo $this->t('Формат'); ?></span>
</div>
<div class="right pd0">
    <div class="words">
        <label for="first_word" class="label"><?php echo $this->t('Слово'); ?> 1</label>
        <input type="text" class="word" name="first_word" value="=>">
        <label for="second_word" class="label"><?php echo $this->t('Слово'); ?> 2</label>
        <input type="text" class="word" name="second_word" value="|">
        <label for="third_word" class="label"><?php echo $this->t('Слово'); ?> 3</label>
    </div>
</div>
</div>
</div>
</div>
<div class="tab tab_2">
    <div class="settings">
        <div class="left pd0">
            <span class="label"><?php echo $this->t('Первым сработает'); ?></span>
        </div>
        <div class="right pd0">
            <select name="syn_pos" class="magic_select">
                <?php
                $options = array(
                    array('_1', $this->t('Синонимайзер')),
                    array('_2', $this->t('Переводчик')),
                );
                _select($options, @Settings::staticGet('synonymsOrder'));
                ?>
            </select>
        </div>
    </div>
</div>
</div>
</div>
</div>
</form>
<script src="dolly_templates/js/jquery-1.12.2.min.js"></script>
<script src="dolly_templates/js/jquery.fancybox.js"></script>
<script src="dolly_templates/js/main.js"></script>
