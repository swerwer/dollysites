<?php class HttpClient
{
    private $_userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';
    private $_info = array();
    private $_infoMime;
    private $_referer;

    public function setReferer($referer)
    {
        $this->_referer = $referer;
        return $this;
    }

    public function getInfo()
    {
        return $this->_info;
    }

    public function userAgent($agent)
    {
        $this->_userAgent = $agent;
        return $this;
    }

    public function getEncode($url = null)
    {
        if ($url) {
            $ch = $this->_curlInit($url);
            @curl_exec($ch);
            $this->_info = @curl_getinfo($ch);
        }
        $charset = @explode('charset=', $this->_info['content_type']);
        if (!isset($charset[1])) {
            $re = "/charset\\S?=[\"|']?(.*)[\"|']/";
            $str = $this->get($url);
            preg_match($re, $str, $matches);
            return @(isset($matches[1])) ? $matches[1] : null;
        }
        return @(isset($charset[1])) ? $charset[1] : null;
    }

    public function getMime($url = null)
    {
        if ($url) {
            $ch = $this->_curlInit($url);
            curl_exec($ch);
            $this->_infoMime = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        }
        $charset = explode(';', $this->_infoMime);
        return (isset($charset[0])) ? $charset[0] : 'html/text';
    }


    public function get($url)
    {
        $ch = $this->_curlInit($url);
        $return = curl_exec($ch);
        $this->_info = curl_getinfo($ch);
        $charset = explode(';', $this->_info['content_type']);
        $this->_infoMime = $charset[0];
        curl_close($ch);
        return $return;
    }

    public function post($url, Array $params = null)
    {
        $ch = $this->_curlInit($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, '', '&'));
        $return = curl_exec($ch);
        curl_close($ch);
        return $return;
    }

    private function _curlInit($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->_userAgent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($this->_referer) {
            curl_setopt($ch, CURLOPT_REFERER, $this->_referer);
        }
        @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        @curl_setopt($ch, CURLOPT_ENCODING , "gzip");
        @curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        @curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        return $ch;
    }
}