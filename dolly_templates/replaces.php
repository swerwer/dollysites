<script type="text/javascript">
    var FIND_WORD = '<?php echo  $this->t('Искомое слово');?>';
    var REPLACE_WORD = '<?php echo  $this->t('Заменить на');?>';
    var SET_TEXT = '<?php echo  $this->t('Введите текст');?>';
    var PREG = '<?php echo  $this->t('Регулярное выражение');?>';
</script>
<?php function printReplace($key=1, $value=array(), $self = null, $first = false) { ?>
    <div class="fields">
        <div class="column" id="column_<?php echo $key;?>">
            <div class="input_wrap">
                <div class="remove"></div>
                <div class="left pd0">
                    <input type="text"
                           name="out[<?php echo $key;?>][l_input]"
                           class="textbox"
                           placeholder="<?php echo $self->t('Введите текст');?>"
                           value="<?php echo @strip_tags($value['l_input']);?>">
                </div>
                <div class="right pd0">
                    <input type="text"
                           name="out[<?php echo $key;?>][r_input]"
                           class="textbox"
                           placeholder="<?php echo $self->t('Введите текст');?>"
                           value="<?php echo @strip_tags($value['r_input']);?>">

                </div>
            </div>
            <div class="textarea_wrap hidden">
                <div class="remove"></div>
                <div class="left pd0">
                    <textarea name="out[<?php echo $key;?>][l_textarea]"
                              class="magic_textarea"
                              placeholder="<?php echo $self->t('Введите текст');?>"><?php echo htmlspecialchars_decode($value['l_textarea']);?></textarea>
                </div>
                <div class="right pd0">
                    <textarea name="out[<?php echo $key;?>][r_textarea]"
                              placeholder="<?php echo $self->t('Введите текст');?>"
                              class="magic_textarea"><?php echo htmlspecialchars_decode($value['r_textarea']);?></textarea>
                </div>
                <div class="change_wrap">
                    <div class="change_type">
                        <input type="checkbox"
                               class="super_checkbox"
                               name="out[<?php echo $key;?>][change_type]"
                               id="regular[<?php echo $key;?>]"
                            <?php echo (@$value['change_type'] == 'preg') ? 'checked' : null;?>>
                        <label for="regular[<?php echo $key;?>]" class="label"><?php echo $self->t('Регулярное выражение');?></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<form action="../admin.php?action=add_replacement&sub=edit"
      method="post"
      id="form" >

    <div class="topbar">
        <h1 class="main-title"><?php echo $this->t('Замены в тексте');?></h1>
        <div class="right button save">
            <?php echo $this->t('Сохранить');?>
        </div>
    </div>
    <div class="text_changer">
        <div id="replaces">
            <div class="fieldtitle" style="padding: 30px 26px 0px 46px;">
                <div class="left">
                    <label for="" class="label" style="font-size: 17px;"><?php echo $this->t('Искомое слово');?></label> <br><br>
                </div>
                <div class="right">
                    <label for="" class="label" style="font-size: 17px;"><?php echo $this->t('Заменить на');?></label> <br><br>
                </div>
            </div>
            <?php if (!sizeof($new)) {
                printReplace(1, null, $this);
            } else {
                if (isset($new) AND is_array($new)) {
                    $i = 0;
                    foreach ($new as $key => $value) {
                        if (@$value['change_type'] == 'script') {
                            continue;
                        }
                        ++$i;

                        printReplace($key, $value, $this);
                    }
                    if (!$i) {
                        printReplace(1, null, $this);
                    }
                }
            } ?>
        </div>

        <div class="buttons">
            <div class="add_column"><?php echo $this->t('Добавить еще одну замену');?></div>
        </div>
    </div>
