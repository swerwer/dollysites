<script type="text/javascript">
    function setSubmit(id, form_id) {
        $.ajax({
            url: '/admin.php?action=set_submit',

            data: ({file: $('#0_file').val(),
                    id: id,
                    form_id: form_id
            }),
            type: 'post',
            success:function (data) {
                if (data == 'SUCCESS') {
                    alert('Success set submit')
                } else {
                    alert('Fail set submit')
                }
            }

        })
        
    }
</script>
<?php
$api = new ServerApiClient();
$formsItems = array();
$url = Settings::staticGet('script_url');
$domain = parse_url($url, PHP_URL_HOST);
foreach ($forms as $key => $form) {
    $formsItems[$key] = array('form' => $form->outertext);
}
$formsItems = $api->formItemsArray($formsItems);
$formsItems = json_decode($formsItems);

foreach($forms as $key => $form) {
    $haventInput = (strpos($form->innertext, '<input') === false);
    $haventTextarea = (strpos($form->innertext, '<textarea') === false);
    if ($haventInput and $haventTextarea) {
        continue;
    }
    ?>
<div class="calculator_item">
    <div class="left col-xs-6 pd0">
        <?php
        $i = 0;

        echo $form->innertext;?>
    </div>
    <div class="right col-xs-6 pd0">
        <div class="select_processor">
            <div class="label"><?php echo $this->t('Обработчик');?></div>


            <input type='hidden' id='<?php echo $key; ?>_old_handler' value='<?php echo $form->action; ?>'>
            <input type='hidden' id='<?php echo $key; ?>_form_id' value='<?php echo $form->d_id; ?>'>
            <input type='hidden' id='<?php echo $key; ?>_file' value='<?php echo $_POST['file']; ?>'>

            <select name="processor" id="<?php echo $key; ?>_handler" class="processor_select magic_select2">
                <option value="this"><?php echo $this->t('Текущий обработчик');?></option>
                <option value="mail"><?php echo $this->t('Отправить на почту');?></option>
                <option value="script"><?php echo $this->t('Передать другому скрипту');?></option>
                <option disabled>----------------------------------</option>
                <?php
                @$handlers = file("./{$this->_parser->cacheDir()}/handlers");
                if ($handlers) {
                    foreach ($handlers as $handler) {
                        $handler = trim($handler); ?>
                        <option value="<?php echo $handler; ?>"
                            <?php if (strpos($form->action, "{$handler}.php")) {echo 'selected';} ?>>
                            <?php echo $handler; ?>
                        </option>
                    <?php }
                } ?>
            </select>

            <div class="handler handler_email" id="<?php echo $key;?>_mail_handler">
                <div class="col">
                        <label for="email" class="label">E-mail:</label>
                        <input type="text" class="input" id="<?php echo $key; ?>_mail" name="email">
                    </div>
                    <div class="col">
                        <label for="subject" class="label"><?php echo $this->t('Тема');?>:</label>
                        <input type="text"
                               class="input"
                               id="<?php echo $key; ?>_subject"
                               name="<?php echo $key; ?>_subject"
                               value="<?php echo $this->t('Новый заказ');?>">
                    </div>
                    <div class="col">
                        <label for="from" class="label"><?php echo $this->t('От кого');?>:</label>
                        <input type="text" value="admin@<?php echo $domain;?>" class="input" id="<?php echo $key; ?>_from" name="from">
                    </div>
                    <div class="col">
                        <label for="template" class="label"><?php echo $this->t('Шаблон письма');?>:</label>
                        <?php
                        $out = $this->t('#ORDER_INFO#') . ':' . PHP_EOL . $formsItems[$key];
                        $out = $out . PHP_EOL . $this->t('Время отправления') . ': {datetime}';
                        ?>
														<textarea name="template"
                                                                  id="<?php echo $key; ?>_template"
                                                                  rows="<?php echo substr_count($out, PHP_EOL) + 1;?>"
                                                                  class="input">
<?php
echo $out;
?>
                                                        </textarea>
                    </div>
                    <div class="col">
                        <label for="title" class="label"><?php echo $this->t('Название обработчика');?></label>
                        <input type="text" 
                               class="input" 
                               id="<?php echo $key; ?>_alias" 
                               name="title">
                    </div>


                <button class="button btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal<?php echo $form->d_id; ?>">
                    <?php echo $this->t('Выбор кнопки отправления');?>
                </button>
                <br>
                <!-- Modal -->
                <div class="modal fade" id="myModal<?php echo $form->d_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="float: right">&times;</button>
                                <h4 class="modal-title" id="myModalLabel"><?php echo $this->t('Выбор кнопки отправления');?></h4>
                            </div>
                            <div class="modal-body">
                                <table class="table">
                                <?php
                                $items = $form->find(Constants::ITEMS_SELECT);
                                foreach ($items as $k => $item):
                                    $sub = $item->find(Constants::ITEMS_SELECT);
                                    if (sizeof($sub)) {
                                        continue;
                                    }
                                    if (@$form->type == 'hidden') {
                                        continue;
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $item->outertext;?></td>
                                        <td><button onclick="setSubmit(<?php echo $k;?>,<?php echo $key;?>);"><?php echo $this->t('Установить');?></button></td>
                                    </tr>
                                <?php endforeach;?>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->t('Закрыть');?></button>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <button class="button" onclick='formSave("<?php echo $key; ?>", "<?php echo $form->d_id; ?>"); return false;'>
                    <?php echo $this->t('Сохранить');?>
                </button>
            </div>
            <div class="handler handler_scripts" id="<?php echo $key;?>_script_handler">
                    <div class="col">
                        <label for="script" class="label"><?php echo $this->t('Скрипт');?>:</label>
                        <input type="text" class="input" id="<?php echo $key; ?>_script" name="script">
                    </div>
                    <div class="col">
                        <label for="method" class="label">
                            <?php echo $this->t('Метод');?>:
                        </label>
                        <select name="method" id="<?php echo $key; ?>_method" class="magic_select2 method_select">
                            <option value="get">get</option>
                            <option value="post">post</option>
                        </select>
                    </div>
                    <button class="button" onclick='formSave("<?php echo $key; ?>", "<?php echo $form->d_id; ?>"); return false;'>
                        <?php echo $this->t('Сохранить');?>
                    </button>
            </div>

            <div id='<?php echo $key; ?>_other_handler' class="handler handler_other">
                <input type="submit"
                       onclick="formSave('<?php echo $key; ?>', '<?php echo $form->d_id; ?>'); return false;"
                       value="<?php echo $this->t('#USE_HANDLER#'); ?>">
            </div>
        </div>
    </div>
</div>

<?php } ?>

<script src="dolly_templates/js/main.js"></script>
<script src="dolly_templates/js/jquery.formstyler.min.js"></script>
<script>

$('.magic_select2').styler();
</script>
<script src="dolly_templates/js/files.js"></script>
