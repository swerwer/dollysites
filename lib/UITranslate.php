<?php class UITranslate
{
    const DEFAULT_LANG = 'ru';
    private static $_instance;
    private static $_language = 'ru';
    private static $_translateDir = 'languages/';
    private static $_dictionary;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    public static function setLanguage($language)
    {
        $fileName = 'mail-success.html';

        $strings = array(
            'Your order is accepted!',
            'Ваш заказ принят!'
        );

        if ($language == 'en') {
            $str = $strings[0];
        } else {
            $str = $strings[1];
        }

        $file = file_get_contents($fileName);
        $file = str_replace($strings, $str, $file);
        file_put_contents($fileName, $file);

        self::$_language = $language;
    }

    public static function setTranslateDir($dir)
    {
        self::$_translateDir = $dir;
    }

    public static function loadDictionary()
    {
        if (file_exists($fileName = self::$_translateDir . self::$_language . '.lang')) {
            self::$_dictionary = require($fileName);
        } else {
            self::$_dictionary = require(self::$_translateDir . '/' . self::DEFAULT_LANG . '.lang');
        }
    }

    public function t($const, $echo = false)
    {
        if (!self::$_dictionary) {
            self::loadDictionary();
        }
        if ($echo) {
            echo self::$_dictionary[$const];
        }
        return (isset(self::$_dictionary[$const])) ? self::$_dictionary[$const] : $const;
    }
}