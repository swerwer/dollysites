jQuery(function($) {
	var first_step = $('.install_content .first_step'),
		second_step = $('.install_content .second_step'),
		third_step = $('.install_content .third_step'),
		first_label = $('.installation_steps .first'),
		second_label = $('.installation_steps .second'),
		third_label = $('.installation_steps .third');

	// next button trigger
	$('.install_navigation .next.button').click(function() {
		// go to secint step
		if ($('#url_site').val() == '') {
			alert(NOT_SITE_URL)
			return false
		}

		if (first_step.hasClass('active')) {
			first_step.removeClass('active').hide();
			second_step.addClass('active').show();
			second_label.addClass('active');
			$('.button.back').show();
		} else if (second_step.hasClass('active')) {
			second_step.removeClass('active').hide();
			third_step.addClass('active').show();
			third_label.addClass('active');
			$('.button.next, .button.back').hide();
			
			$('.success_text').animate({opacity: 1});
			$.ajax({
				type: 'POST',
				url: $('#form1').prop('action'),
				data: $('#form1').serialize(),
				success: function() {
					$('body').append('<iframe id="backpage" src="/" style="opacity:0;" onload="document.iframeAddBase()"></iframe');

					document.iframeAddBase = function() {
						$('#backpage').contents().find('body').prepend('<base target="_parent">');
						$('#backpage').contents().find('a').attr({'target': '_parent'});
					};

					$('#backpage').css({position: 'absolute',
										top: 0,
										left: 0,
										width: '100%',
										height: '100%',
										border: 'none',
										'-webkit-filter': 'blur(7px)',
										'-moz-filter': 'blur(7px)',
										filter: 'blur(7px)',
										'z-index': '-1'});
					$('#backpage').animate({opacity: 1}, 3000, 'swing', function() {
						$('.success_text_1').hide();	
						$('.success_text_2').fadeIn();						
						setTimeout(function() {
							$(function() {
								$({blurRadius: 7}).animate({blurRadius: 0}, {
									duration: 2000,
									easing: 'swing',
									step: function() {
										$('#backpage').css({
											"-webkit-filter": "blur("+this.blurRadius+"px)",
											"-moz-filter": "blur("+this.blurRadius+"px)",
											"filter": "blur("+this.blurRadius+"px)"
										});
									}
								});
							});
							$('body > *:not(iframe, #info_block)').fadeOut('slow', function() {
								$('#info_block').fadeIn('slow');
							});
						}, 2500);
					});
				}
			});
		}

	});
	$('.install_navigation .back.button').click(function() {
		if (third_step.hasClass('active')) {
			second_step.addClass('active').show();
			third_step.removeClass('active').hide();
		} else if (second_step.hasClass('active')) {
			first_step.addClass('active').show();
			second_step.removeClass('active').hide();
			$('.button.back').hide();
		}

	})

})