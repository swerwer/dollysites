<?php
$apiKeyString = "&mode=api&key={$_GET['key']}";
?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'admin_js.php';?>
    <meta charset="utf-8" />
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>Dolly landings</title>
    <link rel="shortcut icon" href="../dolly_images/dolly_favicon.png" type="image/png">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="../dolly_css/dolly_style.css" rel="stylesheet">
    <script type="text/javascript">
        var string_replace = '<?php echo $this->t('#STRING_REPLACE_TYPE#');?>';
        var preg_replace   = '<?php echo $this->t('#PREG_REPLACE_TYPE#');?>';
    </script>

    <script src="../dolly_js/jquery-1.11.3.min.js"></script>
    <script src="../dolly_js/jquery-ui.min.js"></script>
    <script src="../dolly_js/select.js"></script>



    <script>
        $(function() {
            $( "#navbar" ).tabs({
                collapsible: true
            });
        });
    </script>

    <script src="../dolly_js/tinymce/tinymce.min.js"></script>
    <script src="../dolly_js/jquery.livequery.js"></script>
    <script src="../dolly_js/main.js"></script>
    
    <script type="text/javascript">
        var h = screen.height - 450;
        tinymce.init({
            selector: ".page_1 #html textarea",
            extended_valid_elements : 'head, body, style, link, script, html',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste moxiemanager",
                "fullpage"
            ],

            valid_elements : '*[*]',
            convert_urls : false,
            height : h,
            cleanup : false,
            verify_html : false,
            valid_styles : false,
            mode : "textareas",
            language : '<?php echo (Settings::staticGet('language') == 'ru') ? 'ru': 'en';?>',
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#html').show();
            $('#handlers').hide();

            $('.link_1').click(function() {
                $('#html').show();
                $('#handlers').hide();
            });

            $('.link_2').click(function() {
                $('#html').hide();
                $('#handlers').show();
            });

            $('#file_select').change(function() {
                var file = this.options[this.selectedIndex].text;
                startLoadingAnimation();
                $.ajax({

                    url: 'admin.php?action=get_file<?php echo $apiKeyString;?>',
                    type: 'post',
                    data: ({file: this.options[this.selectedIndex].text, path: this.value}),
                    success: function (data) {
                        $('#input').val(data);
                        tinymce.activeEditor.setContent(data);
                        $('#path').val(file + '.html');
                    }
                });

                $.ajax({
                    url: 'admin.php?action=get_forms<?php echo $apiKeyString;?>',
                    type: 'post',
                    data: ({file: this.options[this.selectedIndex].text, path: this.value}),
                    success: function (res) {
                        $('#handlers').empty();
                        $('#handlers').append(res);

                        stopLoadingAnimation();
                    }
                });
            });

            $('#pages').change(function() {
                startLoadingAnimation();
                var val = $('#pages').val()
                if (val) {
                    $.ajax({
                        url: '/?act_dolly=get_page_for_editor<?php echo $apiKeyString;?>',
                        data: ({page: $('#pages').val()}),
                        type: 'post',
                        success: function (data) {
                            $('#input').val(data);
                            $('#path').val($('#pages').val());
                        },
                        done: function (data) {
                            tinymce.activeEditor.setContent(data);
                        }
                    });


                    $.ajax({
                        url: '/?act_dolly=get_forms<?php echo $apiKeyString;?>',
                        data: ({page: $('#pages').val()}),
                        type: 'post',
                        success: function (data) {
                            $('#handlers').empty();
                            $('#handlers').append(data);
                            stopLoadingAnimation();

                        }
                    });
                }
            });
        });
    </script>
</head>

<body>

<div class="wrapper">
    <div class="navbar navbar-inverse">
        <div id="navbar">
            <div id="editor-handler">
                <div class="page_visible page_visible_2 page_1" data-page="2">
                    <div class="header_page">
                        <div class="header_page_center">
                            <form class="form_step_2">
                                <div class="url_wrap">
                                    <select id = "file_select" class="pages">
                                        <option><?php echo $this->t('#SELECT_FILE#');?></option>
                                        <?php $files = @CacheBackend::getPages();

                                        @asort($files);

                                        $pages = array();
                                        $names = array();
                                        $namesUsed = array();
                                        if (isset($files) AND sizeof($files) > 0) {
                                            foreach ($files as $page) {
                                                $page = explode('|', $page);
                                                if (!in_array(trim($page[0]), $pages)) {
                                                    $pages[] = trim($page[0]);
                                                    $title = trim($page[1]);
                                                    if (in_array($title, $names)) {
                                                        $namesUsed[$title] += 1;
                                                        $title .= "({$namesUsed[$title]})";
                                                    } else {
                                                        $names[] = $title;
                                                    }
                                                    echo '<option value = "' . trim($page[0]) . '">' . $title . '</option>';
                                                }
                                            }
                                        } ?>
                                    </select>
                                </div>
                                <div class="links">
                                    <div class="link link_1">
                                        <input type="radio" name="link" id="link_1" checked="checked"/>
                                        <label for="link_1"><span></span><?php echo $this->t('#EDITOR_STRING#');?></label>
                                    </div>
                                    <div class="link link_2">
                                        <input type="radio" name="link" id="link_2"/>
                                        <label for="link_2"><span></span><?php echo $this->t('#FORM_HANDLER_STRING#');?></label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="page_content">
                        <div id = "html">
                            <form method="post"
                                  action="../admin.php?action=save_page<?php echo $apiKeyString;?>">
                                <input type="hidden" name = "path" id = "path">
                                <textarea id = "input" name="page"></textarea>
                                <div class="form-actions">
                                    <input class="form_submit" type="submit" value="<?php echo $this->t('#SAVE#');?>">
                                </div>
                            </form>
                        </div>

                        <div id = "handlers">
                            <?php include 'view_admin_forms.php'; ?>
                        </div>
                    </div>
                </div>
            </div>