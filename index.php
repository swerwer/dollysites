<?php
require_once 'Constants.php';
require 'lib/Controllers.php';

error_reporting(E_ALL);
@set_time_limit(180);
Controllers::setCharset();

$curl = new HttpClient();
$curl->userAgent(Constants::USER_AGENT);

$dom = new simple_html_dom();
$parser = new Parser($dom, $curl);
$parser->baseDir(dirname(__FILE__));
$parser->setUrl(Settings::staticGet('base_url'));

class IndexController extends Controllers
{
    const START_PAGE_EDITOR = false;

    public function get_encoding()
    {
        echo $this->_parser->getEncode($_POST['url']);
        exit;
    }

    public function t($const)
    {
        return $this->_t->t($const);
    }

    public function create_handler()
    {
        echo $this->_parser->createHandler($_POST);
    }

    public function default_action()
    {
        $this->_parser->parser(@$_SERVER['REQUEST_URI']);
    }

    public function start_dolly()
    {

        $this->_settings->set('baseDir', dirname(__FILE__))->save();
        $this->_parser->getHtaccess();
        include 'dolly_templates/view_new.php';
    }

    public function parse()
    {

        if (!$this->isAdmin()) {
            Controllers::redirect('admin.php?action=login');
        }
        if (!self::START_PAGE_EDITOR) {
            Controllers::redirect('/');
        }
        if ($_GET['main'] == 'true') {
            $data = $_POST;
            $data['language'] = OtherFunctions::returnIfIsset(@Settings::staticGet('language'),
                (isset($_SESSION['language']) ? $_SESSION['language'] : 'ru'));
            if (strpos($data['url'], '//') === false) {
                $data['url'] = "http://{$data['url']}";
            }

            $url = $this->_api->getBaseUrl($data);
            $data['ip'] = $_SERVER['REMOTE_ADDR'];

            $data['cacheBackend'] = $data['cache_adapter'];

            file_put_contents('config.ini', $this->_api->getConfigFile($data));

            $settings = $this->saveCacheSettings();
            $settings->set('cacheBackend', $_POST['cache_adapter'])
                     ->set('cacheLimitType', $_POST['cache_limit_type'])
                     ->save();

            CacheBackend::install();

            $this->_parser->loadMainPage($url);
        }
    }

    public function get_page_for_editor()
    {
        list($page, $pageUrl) = $this->_getPagePath();
        if (!file_exists(urldecode($page) . '.html')) {
            $this->_parser->parsePage(urldecode($this->_settings->get('base_url')) . $pageUrl, urldecode($page));
        }
        $this->returnSavedPage($page, false, 'utf-8');
        exit;
    }

    private function _getPagePath($page = null)
    {
        $page = OtherFunctions::returnIfIsset(trim(($page), @$_POST['page']));
        $page = $this->_parser->paths()->clearStartSlash($page);
        $pageUrl = $page;
        $page = $this->_parser->paths()->handleEndSlashInPath($page);
        if (!$page) {
            $page = 'index';
        }
        return array($this->_parser->paths()->replaceSpecialChars(urldecode($page)),
                     $this->_parser->paths()->replaceSpecialChars(urldecode($pageUrl), true));
    }

    public function get_forms()
    {
        list($page, $pageUrl) = $this->_getPagePath();
        $forms = $this->_dom->load(@file_get_contents(urldecode($page) . '.html'))->find('form');
        include 'dolly_templates/view_admin_forms.php';
    }

    public function save_page()
    {
        $path = $this->_parser->paths()->handleEndSlashInPath(trim($_POST['path'])) . '.html';
        $this->_parser->editorSavePage($_POST['page'], $path);
        Controllers::redirect('/?action=parse');
    }

    public function startAuth()
    {
        if (!$this->isAdmin() AND !is_dir($this->_parser->cacheDir() . '/')) {
            $this->_auth('admin', 'admin', null, false);
        }
    }

    public function lang()
    {
        @session_start();
        @$_SESSION['language'] = $_GET['lang'];
        $settings = new Settings();
        $settings->set('language', $_GET['lang'])->save();
        self::redirect('/');
    }

    public function parser()
    {
        list($page, $pageUrl) = $this->_getPagePath($_SERVER['REQUEST_URI']);
        $page = $this->_parser->_createDirsAndGetFileName($page);
        $baseDomain = $this->_parser->getBaseDomainArray($pageUrl);
        $pageUrl = $this->handleSubDomains($pageUrl, $baseDomain);
        $pageUrl = $this->handleOutDomains($pageUrl);
        if ($this->_parser->equals->fileNotSaved($page)) {
            $this->parsePage($pageUrl, $page);
        } else {
            $this->returnSavedPageNew($page);
        }
    }

    private function handleSubDomains($pageUrl, $baseDomain)
    {
        if (stripos(' ' . $pageUrl, 's__')) {
            list($pageUrl) = $this->_parser->_getPageUrlForSubdomains($pageUrl, $baseDomain);
            return $pageUrl;
        }
        return $pageUrl;
    }

    private function handleOutDomains($pageUrl)
    {
        if (strpos(" {$pageUrl}", 'o__')) {
            $pageUrl = str_replace('o__', 'http://', $pageUrl);
            return $pageUrl;
        }
        return $pageUrl;
    }

    private function parsePage($pageUrl, $page)
    {
        $client = new HttpClient();
        if ($this->_parser->equals->_isRelativePath($pageUrl)) {
            $file = $client->get(urldecode($this->_settings->get('base_url') . $pageUrl));
        } else {
            $file = $client->get($pageUrl);
        }
        $type = $client->getMime();
        $charset = $this->getSiteCharset();
        $page = urldecode($page);
        if ($type !== 'text/html') {
            $this->saveAndPrintFile($page, $file, $type);
        } else {
            $this->parseAndPrintPage($page, $charset, $file, $client, $type);
        }
        @header('refresh:0');
    }

    private function getSiteCharset()
    {
        $charset = Settings::staticGet('charset');
        if (!$charset) {
            $charset = OtherFunctions::returnIfIsset($_POST['charset_site'], 'utf-8');
            return $charset;
        }
        return $charset;
    }

    private function saveAndPrintFile($page, $file, $type)
    {
        file_put_contents(urldecode($this->_parser->paths()->replaceSpecialChars($page)), $file);
        header("Content-type: {$type}", true);
        echo $file;
    }

    private function returnSavedPageNew($page)
    {
        if (file_exists(urldecode($page))) {
            @$type = mime_content_type(urldecode($page));
            if ($type == 'text/plain' OR $type == 'text/x-asm' OR strpos(' ' . $type, 'x-c')) {
                $type = 'text/css';
            }
            if (stripos(urldecode($page), '.ttf') OR stripos(urldecode($page), '.woff')) {
                $type = 'font/opentype';
            }
            header("Content-type: {$type}");
            echo trim(file_get_contents(urldecode($page)));
        } else {
            $this->returnSavedPage($page);
        }
    }

    public function getVersion()
    {
        $lastVersion = $this->_api->getVersion();

        if (strpos($lastVersion, 'ERROR_SLOTS') === 0) {
            $lastVersionParse = explode('|', $lastVersion);
            $versionArray = array('status' => 'ERROR',
                'slots' => (int)trim($lastVersionParse[1]),
                'max_slots' => (int)trim($lastVersionParse[2]),
                'url' => trim($lastVersionParse[3]),
                'url_base' => trim($lastVersionParse[4]),
                'ip' => trim($lastVersionParse[5]));
            return $versionArray;
        }

        if (isset($lastVersion)) {
            $lastVersionParse = explode('|', $lastVersion);
            $lastVersionNum = @(int)$lastVersionParse[1];
            $curFix = @Settings::staticGet('fixVersion');
            $curFix = (int)$curFix;
            if (!$curFix) {
                $settings = new Settings();
                $settings->set('fixVersion', $lastVersionNum)->save();
                $curFix = $lastVersionNum;
            }

            if ($lastVersionNum > $curFix) {
                OtherFunctions::update();
                $settings = new Settings();
                $settings->set('fixVersion', $lastVersionNum);
                $settings->save();
            }
        }
    }
}

@session_start();
$translate = UITranslate::getInstance();
UITranslate::setLanguage(
    OtherFunctions::returnIfIsset(
        Settings::staticGet('language'), 
        (OtherFunctions::returnIfIsset(@$_SESSION['language']))
    )
);

$obj = new IndexController();
$action = OtherFunctions::returnIfIsset(@$_GET['act_dolly']);
if (!$action) {
    if ($parser->equals->siteIsInstalled()) {
        $obj->default_action();
        exit;
    } else {
        if (is_dir($parser->cacheDir() . '/')) {
            #$parser->removeSite();
        }
        Controllers::setCharset('UTF-8');
        $obj->startAuth();
        $obj->start_dolly();
        exit;
    }
} else {
    if (method_exists($obj, $action)) {
        Controllers::setCharset('UTF-8');
        $obj->{$action}();
    }
}