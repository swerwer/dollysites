$(document).ready(function() {
	document.currentElement = false;
	$('body').attr({id: 'dollyeditor_nic', spellcheck: 'false'});
	$('body').prepend('<div id="dollyeditor_top" contenteditable="false"><span class="de_logo">DollySites <span>editor</span></span><div id="nicEditor"></div></div><div id="dollyeditor_bottom" contenteditable="false"><span class="de_element"></span><a href="#" class="de_save">'+SAVE+'</a></div>');
		
	document.getCaret = function(el) {
		if (el.selectionStart) { return el.selectionStart; }
    	else if (document.selection)
             { el.focus();
               var r = document.selection.createRange();
               if (r == null) {return 0;}
               var re = el.createTextRange(),
                   rc = re.duplicate();
               re.moveToBookmark(r.getBookmark());
               rc.setEndPoint('EndToStart', re);
               return rc.text.length;
             } 
    	return 0;
	}
	
	document.insertTextAtCursor = function(el, text, offset) {
	    var val = el.value, endIndex, range, doc = el.ownerDocument;
	    if (typeof el.selectionStart == "number"
	            && typeof el.selectionEnd == "number") {
	        endIndex = el.selectionEnd;
	        el.value = val.slice(0, endIndex) + text + val.slice(endIndex);
	        el.selectionStart = el.selectionEnd = endIndex + text.length+(offset?offset:0);
	    } else if (doc.selection != "undefined" && doc.selection.createRange) {
	        el.focus();
	        range = doc.selection.createRange();
	        range.collapse(false);
	        range.text = text;
	        range.select();
	    }
	}
	
	$.fn.removeClassFull = function(rclass) {
	    $(this).removeClass(rclass);
		if($(this).attr('class') == '')
			$(this).removeAttr('class');
		
		return this;
	};
	
	
	
		
    bkLib.onDomLoaded(function() {
	  document.nic = new nicEditor({fullPanel : true});
	  document.nic.setPanel('nicEditor');
	  document.nic.addInstance('dollyeditor_nic');
	  bkLib.noSelect(document.getElementById('dollyeditor_top'));
	  bkLib.noSelect(document.getElementById('dollyeditor_bottom'));
    });
	
	function printBottomInfo(element) {
		$('#dollyeditor_bottom .de_element').html('<span class="de_navtag" id="currentEl" style="font-weight:bold">' + $(element).prop('tagName') + '</span>');
		$(element).parents().each(function(i) {
			if(this.tagName == 'HTML' || this.tagName == 'BODY')
				return;
			
			var eClass, eId, string;
			string = '&nbsp;';
			if(this.className == '')
				eClass = '';
			else
				eClass = '.' + this.className;
			
			if(this.id == '')
				eId = '';
			else
				eId = '#' + this.id;
			
			if(eId != '' && eClass != '')
				string = eId + ', ' + eClass;
			else
				string = eId + eClass;
			
			$('#dollyeditor_bottom .de_element').html('<span class="de_navtag" id="' + i + '" title="' + string + '">' + this.tagName + '</span> </div> <span class="de_navgt">&gt;</span> ' + $('#dollyeditor_bottom .de_element').html());
		});
	}
	
	// ПАНЕЛЬ: навигация по тегам, навели курсор на тег
	$('body').on('mouseenter', '.de_navtag', function(event) {
		var element = $($(document.currentElement).parents()[event.target.id]);
		
		$(element).addClass('dollyeditor_select');
	});
	
	// ПАНЕЛЬ: навигация по тегам, убрали курсор с тега
	$('body').on('mouseout', '.de_navtag', function(event) {
		var element = $($(document.currentElement).parents()[event.target.id]);

		$(element).removeClassFull('dollyeditor_select');
	});

	
	$(document).click(function(event) {
		var target = $(event.target);
		
		if(target.is('.de_save')) {
			//SAVE PAGE ... return;
			$('body').removeAttr('id spellcheck contenteditable');
			$('body').removeClassFull('nicEdit-selected')
			$('#dollyeditor_top, #dollyeditor_bottom').remove();
			$('[dollyeditor]').remove();
			var query = location.pathname+location.search
			query = query.replace('?dollyeditor', '').replace('&dollyeditor', '').replace('#', '')

            if (query == '/') {
				query = 'index.html'
			} else if (query.substr(-1) == '/') {
				query = query + 'index.html'
			} else if (query.search('.html') == -1) {
				query = query + '.html'
			}

			$.post(
				'/admin.php?action=save_page',
				{
					page:$('html')[0].outerHTML,
					path:query
				}
			);
			// $('html')[0].outerHTML - этот код нужно сохранить
			
		}
		
		// клик по тегам в навигации панели
		if(target.is('.de_navtag')) {
			if(event.target.id == 'currentEl') 
				document.currentNavElement = document.currentElement;
			else 
				document.currentNavElement = $(document.currentElement).parents()[event.target.id];
			
			document.currentNavElementOriginal = document.currentNavElement.outerHTML;
			$(document.currentNavElement).removeClassFull('dollyeditor_select');
			
			if($('#dollyeditor_codeblock').length)
				$('#dollyeditor_codeblock').remove();
			
			$('body').append('<div id="dollyeditor_codeblock" class="dollyeditor_codeblock" contenteditable="false" unselectable="on">' +
				'<textarea></textarea>' +
				'<div id="de_codeblock_manage1">' +
				'<span id="de_codeblock_toreplace">' +APPEND+ '</span>' +
				'</div>' +
				'<div id="de_codeblock_manage2">' +
				'<span id="de_codeblock_cancel">'+CANCEL+'</span>&nbsp;&nbsp;' +
				'<span id="de_codeblock_save">' +SAVE+ '</span>' +
				'</div>' +
				'</div>');
			$('#dollyeditor_codeblock textarea').html(document.currentNavElement.outerHTML);

			return;
		}
		
		if(target.is('#de_codeblock_toreplace')) {
			$('body').append('<div id="dollyeditor_replaceblock" class="dollyeditor_codeblock" contenteditable="false" unselectable="on" style="text-align: center;">' +
				'<textarea class="de_replaceblock_textarealeft"></textarea>' +
				'<span style="position:relative;margin:5px;bottom:75px;">'+ON+'</span>' +
				'<textarea class="de_replaceblock_textarearight"></textarea>' +
				'<span id="de_replaceblock_add1" style="float:right; cursor:pointer; padding-right:7px; padding-top:3px; color:#5a77d1;">' +
				ADD_TO_REPLACES + '</span><br>' +
				'<div style="margin-top:40px;"></div>' +
				'<br><span id="de_replaceblock_add2" style="cursor:pointer;color:#5a77d1;">'+ CUT +'</span>' +
				'<br><br><br>' +
				'<span id="de_replaceblock_cancel" style="cursor:pointer;float:left;color:#5a77d1;">'+BACK+'</span></div>');
			$('.de_replaceblock_textarealeft').html(document.currentNavElementOriginal);
			$('.de_replaceblock_textarearight').html(document.currentNavElement.outerHTML);
			return;
		}
		
		
		if(target.is('#de_replaceblock_add1')) {
			$.post('/admin.php?action=add_replacement&sub=editor',
				{
					'out[1][l_input]': $('.de_replaceblock_textarealeft').val(),
					'out[1][l_textarea]': $('.de_replaceblock_textarealeft').val(),

					'out[1][r_input]': $('.de_replaceblock_textarearight').val(),
					'out[1][r_textarea]': $('.de_replaceblock_textarearight').val(),
				}, function() {
					$('#de_replaceblock_add1').html(SUCCESS).css({color: 'green'});
					setTimeout(function() {$('#dollyeditor_codeblock, #dollyeditor_replaceblock').remove();}, 1000);
				});
			return;
			// Добавить в замены
			// передать аяксом $('.de_replaceblock_textarealeft').val() - это "что заменить", и $('.de_replaceblock_textarearight').val() - это "на что заменить" 
			// после этого выполнить:

		}
		if(target.is('#de_replaceblock_add2')) { // Вырезать на всех страницах (заменить на " ")
			// передать аяксом document.currentNavElementOriginal
			// после этого выполнить:
			$.post('/admin.php?action=add_replacement&sub=editor',
				{
					'out[1][l_input]'   : document.currentNavElementOriginal,
					'out[1][l_textarea]': document.currentNavElementOriginal,

					'out[1][r_input]'   : '',
					'out[1][r_textarea]': '',
				}, function() {
					$('#de_replaceblock_add2').html(SUCCESS).css({color: 'green'});
					setTimeout(function() {$('#dollyeditor_codeblock, #dollyeditor_replaceblock').remove();}, 1000);
					return;

				});
		}
		if(target.is('#de_replaceblock_cancel')) {
			$('#dollyeditor_replaceblock').remove();
			return;
		}
		
		if(target.is('#de_codeblock_cancel')) {
			document.currentNavElement.outerHTML = document.currentNavElementOriginal;
			$('#dollyeditor_codeblock').remove();
			return;
		}
		
		if(target.is('#de_codeblock_save')) {
			$('#dollyeditor_codeblock').remove();
			return;
		}
		
		// очищаем навигацию в панели, если кликнули по элементу, который относится к панели
		if(target.is('#dollyeditor_top') || target.is('#dollyeditor_bottom') || target.is('#dollyeditor_codeblock') || target.is('#dollyeditor_replaceblock') || target.parents('#dollyeditor_top').length || target.parents('#dollyeditor_bottom').length || target.parents('#dollyeditor_codeblock').length || target.parents('#dollyeditor_replaceblock').length) {
			$('#dollyeditor_bottom .de_element').html('');
			return;
		}
		
		
		if(event.target.tagName == 'A')
			event.preventDefault();
		
		// выводим панель навигации по тегам 
		printBottomInfo(event.target);
					
		document.currentElement = event.target;
	});
	
	
	$('body').on('keyup', '#dollyeditor_codeblock textarea', function(e) {
		var textarea = $('#dollyeditor_codeblock textarea')[0];
		if(e.which == 187) { // если в textarea ввели "="
			document.insertTextAtCursor(textarea, '""');
			//$(textarea).selectionRange(document.getCaret(textarea)-2);
			textarea.selectionStart = textarea.selectionEnd = document.getCaret(textarea)-1;
		}
	});
	
	$('body').on('input', '#dollyeditor_codeblock textarea', function() {
		//console.log(document.getCaret($('#dollyeditor_codeblock textarea')[0]));
		
		var tag = document.currentNavElement.tagName;
		var newElement = $($('<sometag>' + $('#dollyeditor_codeblock textarea').val() + '</sometag>').find(tag)[0]).addClass('dollyeditor_newelement');
		$(document.currentNavElement).replaceWith(newElement);
		document.currentNavElement = $('.dollyeditor_newelement').removeClassFull('dollyeditor_newelement')[0];
	});
});