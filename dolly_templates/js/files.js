// function if we select node

var setting = {

    callback: {
        beforeClick: BeforeClick,
        onClick: onClick
    }


};

function onClick(event, treeId, treeNode) {
    if (!treeNode.getParentNode()) {
        var parentdir = treeNode.getPath()[0].name;
    } else {

        // define tree parent folders
        var path = '';
        for (i = 0; i < treeNode.getPath().length; i++) {
            if (i == treeNode.getPath().length - 1) {
                var spacer = '';
            } else {
                var spacer = '/';
            }
            path += treeNode.getPath()[i].name + spacer;
            var parentdir = path;
        }

    }

    $('.bottom .file_link').html(parentdir.replace('//', '/'));
    $('.file_field').html(parentdir.replace('//', '/'));
    $('.bottom .file_bottom').show();

};

function BeforeClick() {
    // $.fancybox.reposition();
};
var zNodes =[

];

$(document).ready(function(){

    try {
        $.fn.zTree.init($("#file_chose_window .files"), setting, zNodes);
    } catch(e) {
    }


});

$('.forms_settings .item').dblclick(function() {
    if ($(this).hasClass('active')) {
        $(this).toggleClass('active');
        $('.forms_settings .item').removeClass('innactive');
        $(this).find('.hidden_settings').toggle();
        $(this).find('.thead').toggleClass('active');
        $(this).find('.typical_nav').show();
        $(this).find('.hidden_nav').hide();
    } else {
        $('.forms_settings .item.active').removeClass('active');
        $('.forms_settings .item').not(this).find('.hidden_settings').hide();
        $('.forms_settings .item').not(this).find('.thead').removeClass('active');
        $(this).removeClass('innactive');
        $('.forms_settings .item').not(this).addClass('innactive');
        $(this).find('.thead').toggleClass('active');
        $(this).toggleClass('active');
        $(this).find('.hidden_nav').toggle();
        $(this).find('.typical_nav').toggle();
        $(this).find('.hidden_settings').toggle();

    }
});
function formSave(key, id) {
    file = $('#' + key + '_file').val()
    $.ajax({
        url: 'index.php?act_dolly=create_handler',
        type: 'post',
        data: ({
            type       : $('#' + key + '_handler').val(),
            mail       : $('#' + key + '_mail').val(),
            template   : $('#' + key + '_template').val(),
            subject    : $('#' + key + '_subject').val(),
            from       : $('#' + key + '_from').val(),
            method     : $('#' + key + '_method').val(),
            script     : $('#' + key + '_script').val(),
            form_id    : id,
            old_action : $('#' + key + '_old_handler').val(),
            page       : file,
            file       : file,
            alias      : $('#' + key + '_alias').val()
        }),
        success: function (data) {

            if ($('#' + key + '_handler').val() == 'mail') {
                $('.processor_select').append('<option value = "' + data + '">' + data + '</option>')
            }
            $('.magic_select2').trigger('refresh');

            $('#' + key + '_handler option[value = "' + data + '"]').attr('selected', true)
            $('#' + key + '_mail_handler').hide();
            $('#' + key + '_script_handler').hide();
            $('#' + key + '_other_handler').hide();

            $('.notifications_window').html('<div class="success_text">'+ HANDLER_SUCCESS_TEXT +'</div>');
            $('.notifications_window').stop().fadeIn(600, function() {
                setTimeout(function() {
                    $('.notifications_window').fadeOut(400);
                }, 4000)
            });

        }
    });
};

$('.forms_settings .item .cancel, .forms_settings .item .edit').click(function() {
    $(this).closest('.item').trigger('dblclick');
});

$('select.magic_select2').on('change', function() {
    if ($(this).val() == 'this') {
        $(this).parent().parent().find('.handler').hide();
    } else if ($(this).val() == 'mail') {
        $(this).parent().parent().find('.handler').hide();
        $(this).parent().parent().find('.handler_email').show();
    } else if ($(this).val() == 'script') {
        $(this).parent().parent().find('.handler').hide();
        $(this).parent().parent().find('.handler_scripts').show();
    } else {
        $(this).parent().parent().find('.handler').hide();
        $(this).parent().parent().find('.handler_other').show();
    }
})


