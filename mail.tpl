<?php
$values = array(
    'email'   => '#MAIL#',
    'subject' => '#SUBJECT#',
    'from'    => '#FROM#',
    'message'    => "#MESSAGE#"
);
mail($values['email'],
     $values['subject'],
     $values['message'],
     "From: {$values['from']}\r\nContent-type: text/plain; charset=utf-8\r\n");
/**
 * Logging to file
 */
$orders = (file_exists('./orders.txt')) ? file_get_contents('./orders.txt') : '';

$orders = array();
if (file_exists('./orders.txt')) {
    $file = file_get_contents('./orders.txt');
    $orders = unserialize($file);
}

$orders[] = array('date'    => date('Y-m-d'),
                  'content' => $values['message'],
                  'status'  => 'in_work');

file_put_contents('./orders.txt', serialize($orders));

header('Location: #MAIL_SUCCESS_PAGE#');