<?php class Settings
{
    public static $INI_FILE = 'config.ini';
    const KEY_FILE = 'key.ini';
    private $_params;

    private function _setParam($key, $value)
    {
        if (!$value) {
            unset($this->_params[$key]);
        } else {
            $this->_params[$key] = $value;
        }
    }

    public function setIniFile($fileName)
    {
        self::$INI_FILE = $fileName;
        return $this;
    }

    private function _loadConfigFile()
    {
        if (!$this->_params) {
            $this->_params = @parse_ini_file(self::$INI_FILE);
        }
        return $this->_params;
    }

    public function get($param = null)
    {
        $this->_loadConfigFile();
        return ($param) ? ((isset($this->_params[$param])) ? $this->_params[$param] : null) : $this->_params;
    }

    public function set($key, $value = null)
    {
        $this->_loadConfigFile();
        if (is_array($key)) {
            foreach ($key as $key => $value) {
                $this->_setParam($key, $value);
            }
        } else {
            $this->_setParam($key, $value);
        }
        return $this;
    }

    public function remove($key)
    {
        return $this->set($key);
    }

    public function save()
    {
        @unlink(self::$INI_FILE);
        foreach (@$this->_params as $key => $value) {
            file_put_contents(self::$INI_FILE, "{$key} = \"{$value}\"" . PHP_EOL, FILE_APPEND);
        }
        return $this;
    }

    public static function staticGet($key)
    {
        $params = @parse_ini_file(self::$INI_FILE);
        return (isset($params[$key])) ? $params[$key] : null;
    }

    public function getKey()
    {
        return @file_get_contents(self::KEY_FILE);
    }
}