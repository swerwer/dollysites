<link type="text/css" rel="stylesheet" href="../dolly_css/tables.css">
<script type="text/javascript">
    $(document).ready(function() {
        $('.handlers').hide();
    });

    function formSave(key, id) {
        startLoadingAnimation();
        $.ajax({
            url: 'index.php?act_dolly=create_handler',
            type: 'post',
            data: ({
                type       : $('#' + key + '_handler').val(),
                mail       : $('#' + key + '_mail').val(),
                template   : $('#' + key + '_template').val(),
                subject    : $('#' + key + '_subject').val(),
                from       : $('#' + key + '_from').val(),
                method     : $('#' + key + '_method').val(),
                script     : $('#' + key + '_script').val(),
                form_id    : id,
                old_action : $('#' + key + '_old_handler').val(),
                page       : '<?php echo @$_POST['path'];?>',
                file       : '<?php echo @$_POST['file'];?>',
                alias      : $('#' + key + '_alias').val()
            }),
            success: function (data) {
                stopLoadingAnimation();
                alert('<?php echo $this->t('#SUCCESS_FORM_SAVE#');?>');

                if ($('#' + key + '_handler').val() == 'mail') {
                    $('.handlers_select').append('<option value = "' + data + '">' + data + '</option>')
                }
                $('#' + key + '_handler option[value = "' + data + '"]').attr('selected', true)
                $('#' + key + '_mail_handler').hide();

            }
        });
    };

    function selectHandler(key)
    {
        var handler =  $('#' + key + '_handler').val()

        $('#' + key + '_mail_handler').hide();
        $('#' + key + '_scripts_handler').hide();
        $('#' + key + '_other_handler').hide();


        switch (handler) {
            case 'mail' :
                $('#' + key + '_mail_handler').show();
                break;
            case 'script' :
                $('#' + key + '_scripts_handler').show();
                break;
            case 'this' :
                break;
            default :
                $('#' + key + '_other_handler').show();
                break;
        }
    };
    </script>

<table class="simple-little-table" cellspacing='0' style="margin-left: 0px; width: 100%">

<?php if (isset($forms)) {
    foreach ($forms as $key => $form) { ?>
        <tr>
            <td>
                <div style="overflow:auto;
                        overflow-y:auto;
                        overflow-x:auto;
                        width: 650;
                        height: 320">
                    <?php echo $form->innertext; ?></div>
            </td>
            <input type='hidden' id='<?php echo $key; ?>_old_handler' value='<?php echo $form->action; ?>'>
            <input type='hidden' id='<?php echo $key; ?>_form_id' value='<?php echo $form->d_id; ?>'>
            <td style="width: 430; height: 320; text-align: center; vertical-align: top">
                <?php echo $this->t('#HANDLER#'); ?> :
                <select name='handler'
                        id='<?php echo $key; ?>_handler'
                        onchange='selectHandler(<?php echo $key; ?>);'
                        class="handlers_select">
                    <option value='this'><?php echo $this->t('#THIS_HANDLER#'); ?></option>
                    <option value='mail'><?php echo $this->t('#MAIL_HANDLER#'); ?></option>
                    <option value='script'><?php echo $this->t('#OTHER_HANDLER#'); ?></option>
                    <option disabled>----------------------------------</option>
                    <?php
                    @$handlers = file("./{$this->_parser->cacheDir()}/handlers");
                    if ($handlers) {
                        foreach ($handlers as $handler) {
                            $handler = trim($handler)?>
                            <option value="<?php echo $handler; ?>"
                                <?php if (strpos($form->action, "/{$handler}.php")) {echo 'selected';} ?>>
                                <?php echo $handler; ?>
                            </option>
                        <?php }
                    } ?>
                </select>

                <div id='<?php echo $key; ?>_mail_handler' class='handlers'>
                    <form action='../index.php?act_dolly=mail_handler'
                          method='post'
                          onsubmit='formSave("<?php echo $key; ?>", "<?php echo $form->d_id; ?>"); return false;'>
                        <table class="handlers_table">
                            <tr>
                                <td>
                                    <?php echo $this->t('#MAIL#'); ?> :
                                </td>
                                <td><input type='text'
                                           name='mail'
                                           id='<?php echo $key; ?>_mail'></td>

                            </tr>
                            <tr>
                                <td>
                                    <?php echo $this->t('#SUBJECT#'); ?> :
                                </td>
                                <td><input type='text'
                                           name='subject'
                                           id='<?php echo $key; ?>_subject'
                                           value = '<?php echo $this->t('#NEW_ORDER#');?>'></td>
                            </tr>
                            <tr>
                            <tr>
                                <td>
                                    <?php echo $this->t('#FROM_STRING#'); ?> :
                                </td>
                                <td><input type='text'
                                           name='from'
                                           id='<?php echo $key;?>_from'
                                           value = 'admin@<?php echo $_SERVER['HTTP_HOST'];?>'></td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $this->t('#MAIL_TEMPLATE#'); ?> :
                                </td>
                                <td>
                    <textarea name='template' style="width: 213px; height: 116px;" id='<?php echo $key; ?>_template'>
<?php echo $this->t('#ORDER_INFO#');?>:

<?php
$client = new HttpClient();
$api = new ServerApiClient();

$out = $api->formItems(array('form' => $form->outertext));
echo $out . PHP_EOL . 'Время отправления: {datetime}';
?></textarea></td>
                            </tr>
                            <tr>

                                <td>
                                    <?php echo $this->t('#ALIAS#'); ?> :
                                </td>
                                <td><input type="text" name="alias" id="<?php echo $key; ?>_alias"></td>
                            </tr>
                        </table>
                        <input type='submit'
                               onclick='formSave("<?php echo $key; ?>", "<?php echo $form->d_id; ?>"); return false;'
                               value='<?php echo $this->t('#SAVE#'); ?>'>
                    </form>
                </div>

                <div id='<?php echo $key; ?>_scripts_handler' class='handlers'>
                    <table class="handlers_table">
                        <tr>
                            <td>
                                <?php echo $this->t('#SCRIPT#'); ?> :
                            </td>
                            <td><input type='text'
                                       name='script'
                                       id='<?php echo $key; ?>_script'></td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo $this->t('#METHOD#'); ?> :
                            </td>
                            <td>
                                <select id='<?php echo $key; ?>_method'>
                                    <option value='post'>post</option>
                                    <option value='get'>get</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <input type='submit'
                           onclick='formSave("<?php echo $key; ?>", "<?php echo $form->d_id; ?>"); return false;'
                           value='<?php echo $this->t('#SAVE#'); ?>'>
                </div>

                <div id='<?php echo $key; ?>_other_handler' class="handlers">
                    <input type="submit"
                           onclick="formSave('<?php echo $key; ?>', '<?php echo $form->d_id; ?>'); return false;"
                           value="<?php echo $this->t('#USE_HANDLER#'); ?>">
                </div>

            </td>
        </tr>
    <?php }
} ?>
    </table>