jQuery(function($) {
    // simulate saving
    $('.button.save').click(function(e) {
        $('#form').submit()
        return true
        //$.ajax({
        //	url: $(this).attr('action'),
        //	type: 'post',
        //	data: {path: $('#path').val(),
        //		   page: $('#input').val()},
        //	success: function() {
        //		$('.notifications_window').html('<div class="success_text">Правки успешно сохранены</div>');
        //		$('.notifications_window').stop().fadeIn(600, function() {
        //			setTimeout(function() {
        //			$('.notifications_window').fadeOut(400);
        //			}, 4000)
        //		});
        //	}
        //})
        //
    })
    // call open file window
    $('.button.open_file').click(function(e) {
        $.fancybox({
            href: '#file_chose_window',
            autoResize: false,
            autoSize: true,
            fitToView:false,
        })
    })
    $('.window .close').click(function() {
        $.fancybox.close();
    })
    $('#select_file_button').click(function() {
        $.fancybox.close();
        function load(file) {
            $.ajax({

                url: 'admin.php?action=get_file',
                type: 'post',
                data: ({file: file, path: this.value}),
                success: function (data) {
                    try {
                        $('.magic_select').magicselect();
                        tinymce.activeEditor.setContent(data);

                    } catch(e) {

                    }

                    $('#input').val(data);
                    $('#path').val(file);
                }
            });

            $.ajax({
                url: 'admin.php?action=get_forms',
                type: 'post',
                data: ({file: file, path: this.value}),
                success: function(data) {
                    $('#forms_container').html(data)
                }
            })
        }
        load($('.file_field').text())

    })


    // display languages list
    $('.languages_switcher .current').click(function() {
        $(this).parent().find('.languages_list').toggle("slow");
        $(this).parent().toggleClass('open');
    })
    // tabs
    if ($('.tabs').length > 0) {

        $('.tabs_selectors li').click(function() {
            tabs = $('.tabs .tabs_content');
            id = $(this).attr('id');

            tabs.find('.tab').hide();
            tabs.find('.'+id).show();

            $('.tabs_selectors li').not(this).removeClass('active');
            $(this).addClass('active');
        })

        $('.tabs_selectors').find('li:first').trigger('click');
    } // if element is exist

    // change select positions on click
    $('.content_editor .change_position').click(function() {
        var curr_first = $('.content_editor .first .magic_select_box').html(),
            curr_second = $('.content_editor .second .magic_select_box').html();
        $('.content_editor .second .magic_select_box').html(curr_first);
        $('.content_editor .first .magic_select_box').html(curr_second);

        $('.magic_select').magicselect();
    })
    try {
        $('.magic_select').magicselect();
    } catch(e) {

    }

    if ($('.super_checkbox#status').prop('checked') == true) {
        $('.options .disabled').hide();
    }
    $('.super_checkbox#status').on('change', function() {
        if ($(this).prop('checked') == true) {
            $('.options .disabled').hide();
        } else {
            $('.options .disabled').show();
        }
    })

    if ($('.super_checkbox#GoogleTranslate').prop('checked') == true) {
        $('.translaters .disabled').hide();
    }
    $('.super_checkbox#GoogleTranslate').on('change', function() {
        if ($(this).prop('checked') == true) {
            $('.translaters .disabled').hide();
        } else {
            $('.translaters .disabled').show();
        }
    })

    // get file name in content window
    $('#upload_files').on('change', function(data) {
        $('[for=upload_files]').html($(this).val());
    })

    // simulato uploading file with animation
    if ($('input[type=file]').length > 0) {
        $('input[type=file]').val(''); // clear input
    }
    $('.upload_button').click(function() {
        var file = $(this).parent().find('input[type=file]').val();
        console.log(file);
        if (file != '') {
            $('#file_uploading').submit();
        } else {
            $('input[type=file] + label').html("Ошибка: Выьерите файл!");
        }
    });
    $('#file_uploading').submit(function(e) {

        var formData = new FormData($('#file_uploading')[0]);
        $.ajax({
            url: '/',
            type: 'post',
            processData: false,

            data: formData,
            beforeSend: function() {
                $('#file_uploading').find('label').addClass('loading');
                $('#file_uploading label').text('Файл загружается...');
            },
            complete: function() {
                $('#file_uploading').find('label').removeClass('loading');
                $('#file_uploading label').text('Готово!');
            }
        })

        e.preventDefault();
    })

    // text change page - changing input to textarea
    // we use document ON because our DOM is changing in real time
    $(document).on('focus', '.text_changer .textbox', function() {
        $('.text_changer .column .input_wrap').show();
        $('.text_changer .column .textarea_wrap').hide();
        $(this).parent().parent().parent().find('.input_wrap').hide();
        $(this).parent().parent().parent().find('.textarea_wrap').show();
        $(this).parent().parent().parent().find('.textarea_wrap [name^=textarea_from]').focus();
    })
    // adding more text fields to changer
    //$('.text_changer .buttons .add_column').click(function() {
    $('.text_changer .fields').each(function() {
        var count = $('.text_changer .fields').length;
        if (count <= 1 ) {
            $('.text_changer .column#column_1').find('.input_wrap').hide();
            $('.text_changer .column#column_1').find('.textarea_wrap').show();
        }
    })
    $('.text_changer .buttons .add_column').click(function(){
        var count = $('.text_changer .fields').length;
        count++;

        var item_t = ' <div class="fields item">\
						<div class="column" id="column_' + count + '">\
							<div class="input_wrap">\
							<div class="remove"></div>\
							<div class="left pd0">                                                            \
							<input type="text"                                                                \
						name="out['+ count +'][l_input]"                                               \
						class="textbox"                                                                       \
						placeholder="' + SET_TEXT + '"                                                           \
						>                                             \
							</div>                                                                            \
							<div class="right pd0">                                                           \
							<input type="text"                                                                \
						name="out['+ count +'][r_input]"                                               \
						class="textbox"                                                                       \
						placeholder="' + SET_TEXT + '"                                                           \		                                                                              \
							>                                                                                 \
							</div>                                                                            \
							</div>                                                                            \
							<div class="textarea_wrap hidden">                                                \
							<div class="remove"></div>                                                        \
							<div class="left pd0">                                                            \
							<textarea name="out['+ count +'][l_textarea]"                              \
						class="magic_textarea"                                                                \
						placeholder="' + SET_TEXT + '"></textarea>             \
						</div>                                                                                \
						<div class="right pd0">                                                               \
							<textarea name="out['+ count +'][r_textarea]"                              \
						placeholder="' + SET_TEXT + '"                                                           \
						class="magic_textarea"></textarea>                  \
						</div>                                                                                \
						<div class="change_wrap">                                                             \
							<div class="change_type">                                                         \
							<input type="checkbox"                                                            \
						class="super_checkbox"                                                                \
						name="out['+ count +'][change_type]"                                           \
						id="regular['+ count +']">                                                                                     \
						<label for="regular['+ count +']" class="label">' + PREG + '</label>                    \
						</div>                                                                                \
						</div>                                                                                \
						</div>                                                                                \
						</div>                                                                                \
						</div>';
        //$('.form_step_3 .form_wrap_2 .item .input_wrap').show();
        //$('.form_step_3 .form_wrap_2 .item .textarea_wrap').hide();
        $('#replaces').append(item_t);

        $('.input_wrap').show();
        $('.textarea_wrap').hide();

        $('.text_changer .column#column_' + count).find('.input_wrap').hide();
        $('.text_changer .column#column_' + count).find('.textarea_wrap').show();
    });
    //$('.form_step_3 .form_wrap_2 .item .delete').live('click', function(){
    //	$(this).parents('.item:first').remove();
    //});
    //});
    //var id =  parseInt($('.text_changer .column:last').attr('id').match(/\d+/)) + 1,
    //	content = $('.text_changer .column:first').html(),
    //	textarea_from = parseInt($('.text_changer .column:last').find('[name^=textarea_from]').attr('name').match(/\d+/)) + 1,
    //	textarea_to = parseInt($('.text_changer .column:last').find('[name^=textarea_to]').attr('name').match(/\d+/)) + 1,
    //	checkbox_id = parseInt($('.text_changer .column:last').find('[name^=regular]').attr('name').match(/\d+/)) + 1;
    //	label_regular = parseInt($('.text_changer .column:last').find('[for^=regular]').attr('for').match(/\d+/)) + 1;
//
    //	$('.text_changer .fields').append('<div class="column" id="column_'+id+'">'+content+'</div>');
    //	// set different textarea names
    //	$(document).find('#column_'+id).find('[name^=textarea_from]').attr('name', 'textarea_from['+textarea_from+']');
    //	$(document).find('#column_'+id).find('[name^=textarea_to]').attr('name', 'textarea_to['+textarea_to+']');
    //	$(document).find('#column_'+id).find('[name^=regular]').attr('name', 'regular['+checkbox_id+']').attr('id', 'regular['+checkbox_id+']');
    //	$(document).find('#column_'+id).find('[for^=regular]').attr('for', 'regular['+label_regular+']');
    //
//
    //	// close all textareas
    //	$(document).find('#column_'+id).children('.input_wrap').show();
    //	$(document).find('#column_'+id).children('.textarea_wrap').hide();
    //	// close all inputes and open current textareas
    //	$('.text_changer .column').find('.input_wrap').show();
    //	$('.text_changer .column').find('.textarea_wrap').hide();
    //	$(document).find('#column_'+id).children('.input_wrap .textbox').trigger('click');
    //	$('#column_'+id).find('.input_wrap').hide();
    //	$('#column_'+id).find('.textarea_wrap').show();
    //	$('#column_'+id).find('.textarea_wrap textarea:first').focus();
//
    //	})
    // make first field opened when page load
    //if ($('.text_changer').length > 0) {
    //	$('.text_changer .column#column_1').find('.input_wrap').hide();
    //	$('.text_changer .column#column_1').find('.textarea_wrap').show();
    //}
    // remove columns in text changer

    $(document).on('click', '.text_changer .remove', function() {
        if ($('.text_changer .column').length > 1) {
            $(this).parent().parent().parent().remove();
        } else {

            $(this).parent().parent().parent().remove();
        }
    })
    // copy all text to input
    $(document).on('keydown', '.text_changer .left .magic_textarea', function() {
        var val = $(this).val();
        $(this).parent().parent().parent().find('.input_wrap .left .textbox').val(val);
    });
    $(document).on('keydown', '.text_changer .right .magic_textarea', function() {
        var val = $(this).val();
        $(this).parent().parent().parent().find('.input_wrap .right .textbox').val(val);
    })

    //// end text changer scripts

    /// main nav ajax requests

    function getPage(name) {
        $.ajax({
            url: '/admin.php?action=get_template&name='+name,
            type: 'post',
            success: function(data) {
                if (data != '') {
                    $('.right-side').html(data);

                } else {
                    $('.notifications_window').html('<div class="error">Ошибка загрузки страницы!</div>');

                }
            },
            error: function(data) {
                $('.notifications_window').html('<div class="error">Ошибка:'+data+'</div>');
            }
        })
    }


    //$('.main_nav a').click(function(e) {
    //			e.preventDefault();
    //			var name = $(this).attr('page-name');
    //
    //            //getPage(name)
    //	        return true;
    //
    //		})

    $('#get_archive').click(function(){
        document.location.href="/admin.php?action=get_site_archive";
    })

    $('#dolly_remove').click(function(){
        confirm = confirm('You sure?');

        if (confirm) {
            document.location.href = '/admin.php?action=remove_site';
        }
    })

    $('#dolly_clear_cache').click(function(){
        confirm = confirm('You sure?')

        if (confirm) {
            document.location.href = '/admin.php?action=clear_site_cache';
        }
    });

})